<?php

function less_parser($value, $group){
	require_once 'lessc.inc.php';

	$less = new lessc();
	$value = $less->parse($value);

	return $value;
}
