<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="css/images/favicon.ico" />
  <title>Dentyne</title>
  
<?php
  
  $this->template->load_asset('css','common/jquery-ui');
  $this->template->load_asset('css','validationEngine.jquery');

  echo $this->template->render_assets('css');
?>
  
<!--[if IE]>
<style type="text/css">
@font-face {
    font-family: 'franklin_gothic_stdXCn';
    src: url('franklingothicstd-extracond-webfont-webfont.eot');
    src: url('franklingothicstd-extracond-webfont-webfont.eot?#iefix') format('embedded-opentype'),
         url('franklingothicstd-extracond-webfont-webfont.woff') format('woff'),
         url('franklingothicstd-extracond-webfont-webfont.ttf') format('truetype'),
         url('franklingothicstd-extracond-webfont-webfont.svg#franklin_gothic_stdXCn') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'tradegothicboldcondtwenty';
    src: url('tradegothic-boldcondtwenty-webfont-webfont.eot');
    src: url('tradegothic-boldcondtwenty-webfont-webfont.eot?#iefix') format('embedded-opentype'),
         url('tradegothic-boldcondtwenty-webfont-webfont.woff') format('woff'),
         url('tradegothic-boldcondtwenty-webfont-webfont.ttf') format('truetype'),
         url('tradegothic-boldcondtwenty-webfont-webfont.svg#tradegothicregular') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'tradegothiccondeighteen';
    src: url('tradegothic-condeighteen-webfont-webfont.eot');
    src: url('tradegothic-condeighteen-webfont-webfont.eot?#iefix') format('embedded-opentype'),
         url('tradegothic-condeighteen-webfont-webfont.woff') format('woff'),
         url('tradegothic-condeighteen-webfont-webfont.ttf') format('truetype'),
         url('tradegothic-condeighteen-webfont-webfont.svg#tradegothicregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
</style>

<![endif]-->

<link rel="stylesheet" href="/css/fonts.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="/css/jquery.countdown.css" />
<link rel="stylesheet" type="text/css" href="/css/additional_style.css" />

<?php if($this->session->userdata('language') && $this->session->userdata('language') == 'french') { ?>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45266312-7', 'gagnezavecdentyne.ca');
  ga('send', 'pageview');

  </script>
<?php } else { ?>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45266312-6', 'winwithdentyne.ca');
  ga('send', 'pageview');

  </script>

<?php } ?>

</head>
<body class="<?php echo $body_class ?>">
<div class="wrapper">
  <div class="container <?php echo $french_background ?>">
    <div class="shell">