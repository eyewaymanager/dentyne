	</div><!-- /.wrapper -->
	<footer>
	<?php
	$selected_language = $this->session->userdata('language');
	//jquery library and jquery UI library
	 $this->template->load_asset('js','vendor/jquery-1.11.0.min');

	 //$this->template->load_asset('js','jquery/jquery-ui-1.8.18.min');
	
	 $this->template->load_asset('js','facebook/functions');
	 $this->template->load_asset('js','facebook/main');
	 
	 
	 $this->template->load_asset('js','facebook/jquery.validationEngine');

	 //Load Validation engine language files according to the selected language
	 if($this->session->userdata('language') && $this->session->userdata('language') == 'french')
	 	$this->template->load_asset('js','facebook/jquery.validationEngine-fr');
	 else
	 	$this->template->load_asset('js','facebook/jquery.validationEngine-en');
	 
	 $this->template->load_asset('js', 'facebook/jquery.mask');
	 $this->template->load_asset('js', 'jquery.placeholder');
	 $this->template->load_asset('js', 'jquery.autotab.min');
	 $this->template->load_asset('js', 'backfix.min');
	 echo $this->template->render_assets('js');
	?>
	
	<script type="text/javascript" src="js/jquery.plugin.js"></script> 
	<script type="text/javascript" src="js/jquery.countdown.js"></script>
	
	</footer>

</body>
</html>