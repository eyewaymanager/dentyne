<?php
$redirect = uri_string();
	$toggle_language = $this->session->userdata('language') == 'english' || !$this->session->userdata('language') ? 'french' : 'english';
?>
<div class="clearfix"></div>
<div class="footer">
		<div class="shell">
			<div class="footer-nav">
				<ul>
					<li>
						<a href="/home/prize?language=<?=$language?>&step=<?=$step?>&facebook=<?php echo $facebook ?>"><?php echo lang('prize_details') ?></a>
					</li>

					<li>
						<a href="/home/rules?language=<?=$language?>&step=<?=$step?>&facebook=<?php echo $facebook ?>"><?php echo lang('official_rules') ?></a>
					</li>

					<li>
						<a href="<?php echo lang('terms_link') ?>" target="_blank"><?php echo lang('terms') ?></a>
					</li>


					<li>
						<a href="<?php echo lang('contact_us_link') ?>"><?php echo lang('contact_us') ?></a>
					</li>

					<li>
						<a href="/home/faq?language=<?=$language?>&step=<?=$step?>&facebook=<?php echo $facebook ?>"><?php echo lang('faq') ?></a>
					</li>

					<li>
						<a href="home/select_language/<?php echo $toggle_language ?>?step=<?=$step?>&redirect=<?php echo $redirect ?>&facebook=<?php echo $facebook ?>" id="language_selector"> <?php echo lang($toggle_language) ?></a>
					</li>
				</ul>
			</div><!-- /.footer-nav -->
		</div><!-- /.shell -->
	</div>
	</div><!-- /.footer -->
