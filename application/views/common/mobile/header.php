<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
  ______        __          __                                             
 |  ____|       \ \        / /                                             
 | |__  _   _  __\ \  /\  / /_ _ _   _                                     
 |  __|| | | |/ _ \ \/  \/ / _` | | | |                                    
 | |___| |_| |  __/\  /\  / (_| | |_| |                                    
 |______\__, |\___| \/  \/ \__,_|\__, |                                    
         __/ |                    __/ |                                    
  _     |___/                    |___/                                     
 (_)      / _|       ____                                                  
  _ _ __ | |_ ___   / __ \  ___ _   _  _____      ____ _ _   _   __ _ _ __ 
 | | '_ \|  _/ _ \ / / _` |/ _ \ | | |/ _ \ \ /\ / / _` | | | | / _` | '__|
 | | | | | || (_) | | (_| |  __/ |_| |  __/\ V  V / (_| | |_| || (_| | |   
 |_|_| |_|_| \___/ \ \__,_|\___|\__, |\___| \_/\_/ \__,_|\__, (_)__, |_|   
                    \____/       __/ |                    __/ |  __/ |     
                                |___/                    |___/  |___/      
 -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta charset="utf-8">
	<title><?php echo isset($this->settings_array->title) ? $this->settings_array->title : '' ?></title>
<?php
	//Main CSS file 
	$this->template->load_asset('css','common/mobile/main');
	//jquery UI CSS file
 	$this->template->load_asset('css','common/jquery-ui');
 	//Bootstrap CSS files
 	$this->template->load_asset('css','bootstrap.min');
 	$this->template->load_asset('css','bootstrap-responsive.min');
 	$this->template->load_asset('css','bootstrap-tagsinput');

 	echo $this->template->render_assets('css');
?>
</head>
<body>