<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta charset="utf-8">
	<title><?php echo isset($this->settings_array->title) ? $this->settings_array->title : '' ?></title>
<?php
	//Main CSS file 
	$this->template->load_asset('css','common/admin/main');
	//jquery UI CSS file
 	$this->template->load_asset('css','common/jquery-ui');
 	//Bootstrap CSS files
 	$this->template->load_asset('css','bootstrap.min');
 	$this->template->load_asset('css','bootstrap-responsive.min');
 	$this->template->load_asset('css','bootstrap-tagsinput');
  $this->template->load_asset('css','styles/kendo.common.min');
  $this->template->load_asset('css', 'styles/kendo.silver.min');

 	echo $this->template->render_assets('css');
?>
</head>
<body>