	<footer>
	<?php
	//jquery library and jquery UI library
	 $this->template->load_asset('js','vendor/jquery-1.9.1.min');
	 $this->template->load_asset('js','jquery/jquery-ui-1.8.18.min');
	 $this->template->load_asset('js', 'jquery/jquery.inview');
	 $this->template->load_asset('js','jquery/jquery.smooth-scroll.min');
	 $this->template->load_asset('js','vendor/modernizr-2.6.2-respond-1.1.0.min');
	//jquery plugins section added
	 $this->template->load_asset('js','vendor/bootstrap.min');
	 $this->template->load_asset('js','plugins/jquery.backstretch.min');
	 $this->template->load_asset('js','admin/main');
	 $this->template->load_asset('js','jquery/bootstrap-tagsinput.min');
	 $this->template->load_asset('js', 'kendo.all.min');
	// $this->template->load_asset('js', 'page_scroller/page_scroller');

	 echo $this->template->render_assets('js');
	?>
	</footer>
</body>
</html>