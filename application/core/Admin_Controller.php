<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Basic Admin controller. All admin area controllers inherit from it
 * Extends MY_Controller
 */
class Admin_Controller extends MY_Controller {


	public function __construct() {
		parent::__construct();

		//We initialize logged in and user role variables
		$this->data['is_admin'] = FALSE;
		$this->data['is_user'] = FALSE;
		$this->data['logged_in'] = FALSE;

		//We load auth module which handles user authentication and role confirmation
		if(!class_exists('auth'))
			$this->load->module('auth/auth');

		//We check if the user is already logged in and set logged_in variable
		if($this->tank_auth->is_logged_in())
			$this->data['logged_in'] = TRUE;

		$this->template->set_template('admin_template');
	}
}