<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

//We are loading MX library for HMVC functionality
//From this controller all controllers will inherit
require APPPATH."third_party/MX/Controller.php";


/**
 * Basic Controller from which all other controllers will inherit
 */
class MY_Controller extends MX_Controller {
	//Global variable holding logged in bolean.
	public $logged_in = FALSE;

	//Global variable holding Facebook signed_request param
	public $signed_request = '';

	//Global variable holding Facebook page ID
	protected $fb_page_id = '';

	//Global variable holding Facebook liked boolean
	protected $fb_liked;

	//Global variable holding Facebook admin boolean
	protected $fb_admin = '';

	//Global variable holding Facebook user age param
	protected $fb_user_age = '';

	//Global variable holding language selected
	public $selected_language = 'english';

	public $ajax_debug = FALSE;

	public $is_facebook ;

	public $step ='';
	
	public $session_signed_request;

	public function __construct() {
		parent::__construct();

		
		$this->template->set_template('default');
		//$this->session->set_userdata('is_facebook', false);
		//Check to see if this the page loads as a facebook app or not.
		
		if($this->input->get('step'))
			$this->step = $this->input->get('step');

		if($_SERVER['SERVER_NAME'] == 'www.gagnezavecdentyne.ca' || $_SERVER['SERVER_NAME'] == 'gagnezavecdentyne.ca')
			$this->session->set_userdata('language', 'french');
			
		if(!$this->input->is_ajax_request())
			$this->session->set_flashdata('redirect', uri_string());


		//If facebook library is not loaded we load it here
		if(!class_exists('tank_auth'))
			$this->load->library('auth/tank_auth');
		
		//We are fetching signed_request param from facebook request and 
		//assign it to signed_request variable.
				
		if($this->input->get('language'))
			$this->session->set_userdata('language', $this->input->get('language'));
		//Finding selected language from session
		
		$this->selected_language = $this->session->userdata('language') ;
	}

	protected function init_language()
	{
		//Loding language file
		$this->lang->load('site_language', $this->selected_language);
		
		//Save calling page URI to session for redirection 
		//to the page after language change
		//$this->session->set_flashdata('redirect_uri', uri_string());

		if($this->selected_language == 'french')
			$this->data['french_background'] = 'fr';
		else 
			$this->data['french_background'] = '';
	}

	/**
	 * Ajax handler for ajax output
	 * DEPRECATED - Use the ajax template
	 */
	public function ajax_output($data , $total= null, $status = TRUE , $js_eval = FALSE)
	{
		//Set Header
		header('Content-Type: application/json');
	
		//Set Return array
		$return = array();
		$return['data'] = $data;
		$return['total'] = $total;
		$return['status'] = ($status ? "SUCCESS" : "FAIL" );
		$return['js_eval'] = ( ! empty($js_eval) ? $js_eval : "" );
	
		//Check for debugging
		if($this->ajax_debug){
			if(isset($this->db)){
				$return['debug_sql'] = $this->db->last_query();
			}
			if(isset($this->session)){
				$return['debug_session'] = $this->session->userdata();
			}
		}
	
		//Output and die
		echo json_encode($return); die();
	}

}