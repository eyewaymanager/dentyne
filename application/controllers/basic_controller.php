<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Basic_Controller extends MY_Controller {


	public function __construct() {
		parent::__construct();
	}

	public function select_language($language) {
		
		$redirect = $this->input->get('redirect');
			
		//var_dump($this->session->userdata('is_facebook'));
		if($this->input->get('facebook') == '') {
			if(trim($language) == 'french')
				redirect('http://www.gagnezavecdentyne.ca/'.$redirect);
			else if(trim($language) == 'english')
				redirect('https://www.winwithdentyne.ca/'.$redirect);
		}

		else {
			$this->session->set_userdata('language', trim($language));
			redirect('https://www.winwithdentyne.ca?facebook='.$this->input->get('facebook').'&language='.$language);
		}
	}
}