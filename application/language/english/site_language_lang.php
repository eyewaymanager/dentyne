<?php
//English translation file
$lang['english'] = 'English';
$lang['french'] = 'Français';
$lang['share_text'] = 'share contest with your friends so they can enter too.';
$lang['enter_now'] = 'enter now';
$lang['congratulations'] = 'Congratulations! ';
$lang['mobile_navigation_button_text'] = 'Mobile Navigation Button';
$lang['twitter_share_text'] = 'I just entered for a chance to win a VIP trip to Hollywood! For deets & to enter, go to www.winwithdentyne.ca/officialrules';
$lang['twitter_url'] = '';
$lang['contact_us'] = 'Contact Us';
$lang['faq'] = 'FAQ';
$lang['contact_us_link'] = 'mailto: contest@winwithdentyne.ca';
$lang['terms_link'] = 'http://brands.nabisco.com/MiscContent/ca/Terms_of_use_en.aspx';
$lang['terms'] = 'Terms & Conditions';
$lang['coming_soon_title'] = 'CONTEST coming SOON';
$lang['coming_soon_text'] = 'Please check back on <span>JUNE 3, 2014</span>';
//Personal info form
$lang['enter_your_upc_code'] = 'Enter your UPC*';
$lang['enter_your_birthday'] = 'Enter your date of birth*';
$lang['enter_your_information'] = 'Enter your information';
$lang['required_fields'] = '*Required fields';
$lang['enter_words_bellow'] = 'ENTER THE LETTERS THAT APPEAR BELOW';
$lang['submit'] = 'Submit';
$lang['prize_details'] = 'Prize Details';
$lang['official_rules'] = 'Official Rules';
$lang['rules_accept_text'] = 'I have read and confirm compliance with the <a href="/home/rules" target="_blank">Official Rules</a>.';
$lang['newsletter_accept_text'] = 'I would like to be contacted about future promotions, offers and product news from <span>Dentyne</span> and/or any Mondelēz Canada brand.';
$lang['form_hint'] = 'Limit one (1) Entry per calendar day. <br /> Only one (1) email address permitted <br /> to enter.';
$lang['upc'] = 'UPC*';
$lang['first_name'] = 'First Name*';
$lang['last_name'] = 'Last Name*';
$lang['email'] = 'Email*';
$lang['postal_code'] = 'Postal Code*';
$lang['telephone'] = 'Telephone (Optional)';
$lang['reload'] = 'Reload';


$lang['birth_dd'] = 'dd';
$lang['birth_mm'] = 'mm';
$lang['birth_yy'] = 'yyyy';

$lang['optional'] = 'optional';

//Success page
$lang['success'] = 'Success!';
$lang['success_text'] = 'An email has been sent to the email address you\'ve provided us <br /> with instructions on how to claim your prize. Please check your email and spam filters.<br /> Remember you are also eligible for our grand prize draw!';


//sorry page
$lang['sorry'] = 'Sorry';
$lang['skill_error_text'] = 'You\'ve answered the skill testing question incorrectly <br /> so you are no longer eligible to win the prize. <br /> You can enter once per calendar day so remember to come back tomorrow!';

//sorry page not in sequence
$lang['winning_error_text'] = 'You didn\'t win an Instant Win Prize but are still eligible for the Grand Prize draw. <br>You can enter once per day so remember to come back tomorrow! ';

//Skill test page
$lang['skill_test_text'] = 'You are eligible to win two (2) movie tickets. <br />Please correctly answer the skill testing question below <br /> without assistance of any kind to claim your prize. <br /> You have 5 minutes to answer the question.';

//Rules page
$lang['rules'] = 'Rules';
$lang['rules_text'] = '<h1>Official Contest Rules</h1>
<p><strong>1) CONTEST PERIOD: </strong>The DENTYNE Practice Safe Breath At The Movies contest (the “Contest”) begins on June 3, 2014 at 12:00 p.m. (noon) Eastern Time (“ET”) and ends on August 26, 2014 at 12:00 p.m. (noon) ET (the &quot;Contest Period&quot;). </p>
<p><strong>2) SPONSOR:</strong> Mondelez Canada Inc., 2660 Matheson Blvd. East, Mississauga, ON L4W 5M2 (the “Sponsor”). </p>
<p>ADMINISTRATOR: BSTREET Communications Inc., 40 Holly Street, Suite 700, Toronto, ON, M4S 3C3 (the “Administrator”).</p>
<p><strong>3) HOW TO ENTER: NO PURCHASE NECESSARY</strong>. To enter the Contest, you will require the UPC (as defined below) from any one (1) of the Participating Products (as defined below). Enter using the methods of entry outlined below. No Entries will be accepted by any other means. For the purposes of this Contest and these Official Contest Rules, an Entry shall include the Entry Form and the UPC, as each term is defined herein. LIMIT one (1) Entry per person per calendar day during the Contest Period, regardless of method of entry.</p>
<p><strong>3A) UPC ENTRY ONLINE:</strong> To enter, visit <a href="http://www.WinWithDentyne.ca">www.WinWithDentyne.ca</a> or <a href="http://www.facebook.com/DentyneCanada">www.facebook.com/DentyneCanada</a> (each and collectively, the “Contest Website”) and follow the on screen instructions to complete the entry form (the “Entry Form”) including providing your first and last name, postal code, email address, telephone number and date of birth. You will then need to  enter one (1) ten (10) digit UPC number (the &ldquo;UPC&rdquo;) from any one (1) of the  Participating Products listed below. Once completed, you must agree to the Official Contest Rules, enter the CAPTCHA code to verify you’re not a robot and click “Submit” and you will receive one (1) entry (each an “Entry” and collectively, the “Entries”) in to the Contest to be eligible to win both the Grand Prize and an Instant Win Prize (both as defined below). </p>
<p>All fields on the Entry Form must be completed unless indicated as optional. If you have been selected to win an Instant Win Prize, an Instant Win Notification (as defined below) of this will be instantly displayed along with further instructions on how to claim the Instant Win Prize. See Rule 6 below for further details. Entrant may only use one (1) email address and one (1) Facebook account to enter the Contest. All selected entrants are subject to verification before any Prize will be awarded. All Entries become the sole property of Sponsor and will be acknowledged or returned. If it is discovered that you entered or attempted to enter more times than permitted under these Official Contest Rules, you may, at Sponsor’s sole discretion, be disqualified from the Contest and forfeit any Prizes. </p>
<p> <strong>3B) NO PURCHASE ENTRY: </strong>To enter without purchase, write down the UPC from one (1) of the Participating Products listed in below while in store and then enter the Contest as described in 3A above. </p>
<p><strong>PARTICIPATING PRODUCTS: </strong>The following is a list of participating products (each and collectively, “Participating Products”). Availability of Participating Products is subject to participating store listings. NOTE: To enter the Contest, you will require the UPC from any one (1) of the Participating Products listed below. For No Purchase Entry see Rule 3B above.<br>
  <br>
  Participating Products:<br>
  DENTYNE ICE Spearmint Bottles <br>
  DENTYNE ICE Peppermint Bottles <br>
  DENTYNE ICE INTENSE Bottles <br>
  DENTYNE FIRE Bottles <br>
  DENTYNE ICE MIDNIGHT MINT Bottles <br>
  DENTYNE ICE AVALANCHE Bottles <br>
  DENTYNE RUSH Peppermint Bottles<br>
  DENTYNE RUSH Spearmint Bottles</p>
<p><strong>4) ELIGIBILITY: </strong>Contest is open to legal residents of Canada who are sixteen (16) years of age or older at the time of entry. Employees of Mondelez Canada Inc., (“Sponsor”), BSTREETCommunications Inc., Cineplex Entertainment LP, and their respective parents, affiliates, subsidiaries, related companies and advertising and promotion agencies, (collectively, “Promotion Parties”), and the household members (whether or not related) and/or the immediate family members (defined as husband, wife, spouse, mother, father, brother, sister, son and/or daughter, whether or not they reside in the same household) of any of the above are not eligible to enter the Contest. </p>
<p>If the selected entrant is over the age of sixteen (16), but has not reached the age of majority in the province or territory in which he or she resides, the authorization of his or her parent or legal guardian will be required to accept the Prize as awarded, including the execution of the Release (as defined below).</p>
<p><strong>5) POTENTIAL WIN ON AND NOTIFICATION: </strong><br>
  <strong>GRAND PRIZE: </strong>A random draw will be made by Administrator on August 27, 2014 at 12:00 p.m. (noon) ET at the Administrator\'s address listed in Rule 2, from all eligible Entries received during the Contest Period. </p>
<p>The selected entrant will be notified by email and/or telephone (the “Grand Prize Notification”) within forty-eight (48) hours of the draw at the email address and/or telephone number provided by entrant at time of entry with further instructions on how to claim the Grand Prize. Please check your email and spam filters. Selected entrant must reply to the Grand Prize Notification within seventy-two (72) hours of the Grand Prize Notification being sent or selected entrant will forfeit his/her right to claim the Grand Prize and Sponsor, in its sole discretion, may select an alternative entrant for the Grand Prize, who will be subject to disqualification in the same manner or cancel the Grand Prize. Sponsor is not responsible for the failure, for any reason whatsoever, of a selected entrant to receive the Grand Prize Notification or for Sponsor to receive a selected entrant’s response.</p>
<p><strong>INSTANT WIN PRIZES: </strong>One thousand (1,000) random, computer-generated times, each associated with one (1) of the Instant Win Prizes listed below, will be generated during the Contest Period. If an entrant submits an Entry at the exact time (or the first time thereafter) of one (1) of the random, computer-generated times, he/she will be instantly notified (the “Instant Win Notification”) if he/she has been selected to win an Instant Win Prize. The Instant Win Notification will contain further information on how to claim the Instant Win Prize. For the avoidance of doubt, Instant Win Prize winners will remain eligible to win the Grand Prize. </p>
<p><strong>6) PRIZE CLAIM CONDITIONS:</strong><br>
Before being declared a winner, the selected entrant must correctly answer a time-limited mathematical skill-testing question without assistance or mechanical or electronic aid, administered: (i) on the Contest Website with the Instant Win Notification (for Instant Win Prizes) or (ii) at a mutually agreeable time by telephone (for Grand Prize), and he/she (or his/her parent/legal guardian, if entrant is under the age of majority in his/her province/territory of residence) may be required to sign and return a Declaration and Release form (the “Release”) confirming compliance with the Official Contest Rules, acceptance of the Prize as awarded and releasing the Releasees (as defined below) from and against any and all liability in connection with the Contest, including the acceptance, possession, use and/or misuse of any Prize. The selected entrant must return the signed Release within the timeframe specified by Sponsor and/or its agent or the Prize may be forfeited, at Sponsor’s sole discretion. If a selected entrant fails to respond to the Grand Prize Notification or Instant Win Notification (as applicable), is not in compliance with these Official Contest Rules, incorrectly answers the skill-testing question, refuses to accept the Prize, email correspondence between Sponsor and selected entrant is returned as undeliverable, or fails to return the Release (and any other documents, as applicable) within the required timeframe, the Prize may not be awarded, at Sponsor’s sole discretion, and the selected entrant hereby releases the Releasees from and against any and all liability in connection with the Contest; including, the acceptance, possession, use and/or misuse of any Prize. Only three (3) alternate drawings for the Grand Prize may be held (and such selected entrants will be subject to disqualification in the manner as set forth herein), after which the Grand Prize will remain unawarded, at Sponsor’s sole discretion. Instant Win Prizes will be awarded approximately four (4) to six (6) weeks after selected entrant of Instant Win Prize has been verified as a winner. </p>
<p><strong>7) ODDS: </strong><br>
  <strong>GRAND PRIZE:</strong> The odds of being selected to win the Grand Prize will depend upon the total number of eligible Entries received during the Contest Period.</p>
<p><strong>INSTANT WIN PRIZES: </strong>The odds of being selected to win an Instant Win Prize will depend on the date and time of entry. </p>
<p><strong>8) PRIZES/APPROXIMATE RETAIL VALUES: </strong>For the purposes of this Contest and these Official Contest Rules, unless otherwise noted, a “Prize” shall include both the Grand Prize and the Instant Win Prize, as each term is defined herein. </p>
<p><strong>GRAND PRIZE: </strong>There will be one (1) grand prize (the “Grand Prize”) available to be won consisting of one (1) trip for two (2) to Hollywood, California, including the following:<br>
  •  roundtrip coach-class air transportation for two (2) from the international airport closest to winner\'s residence    (as determined by Sponsor at its sole discretion) to Hollywood, California;<br>
  •  two (2) consecutive nights\' accommodations at a hotel determined by Sponsor at its sole discretion    (based on one (1) standard room with double occupancy); <br>
  •  return ground transportation between Los Angeles World Airports (LAX) and hotel; and<br>
  •  $1,000 CAD spending money for the winner. </p>
<p>Total approximate retail value (&quot;ARV&quot;) of the Grand Prize: $7,500 based on sample Toronto departure. Actual value may vary based on date of departure, date of travel, airfare fluctuations and exchange rates. Winner will not be entitled to the difference, if any, between the stated and actual value. Grand Prize trip anticipated to occur before December 31, 2014. Winner and travelling companion must be available to travel during this timeframe, or Grand Prize may be forfeited in its entirety, and, at Sponsor’s sole discretion awarded to an alternate selected entrant who will be subject to disqualification in the same manner. All details of the Grand Prize shall be determined by Sponsor in its sole discretion. Absolutely no changes to travel dates or passenger names are permitted once the booking has been made and the Grand Prize winner has submitted the name of his/her travelling companion and all related booking information to Sponsor. Travel must be round trip. Sponsor will determine airline and flight itinerary in its sole discretion. No refund or compensation will be made in the event of the cancellation or delay of any flight. Travel is subject to the terms and conditions set forth in these Official Contest Rules, and those set forth by the Sponsor’s airline carrier of choice as detailed in the passenger ticket contract. All expenses and incidental travel costs not expressly stated in the package descriptions above, including but not limited to, ground transportation, meals, incidentals, gratuities, passenger tariffs or duties, airline fees, surcharges, airport fees, service charges or facility charges, personal charges at lodging, security fees, taxes or other expenses are the responsibility solely of winner and his/her travel companion. If winner is under the age of majority in his/her province/territory of residence, he/she must travel with and be accompanied at all times by his/her parent/legal guardian. In such a case, the parent/legal guardian of the minor must be prepared to produce any written consent or consents from the minor’s other parent/legal guardian required by Sponsor to permit the minor to travel. Unless child of winner, travel companion must be age of majority as of the date of departure. Travel companion must travel on same itinerary and at the same time as the winner. Travel companion (or if travel companion is a minor in his/her province/territory of residence, his/her parent/legal guardian) must execute a Release prior to issuance of travel documents. Prior to travel, winner and travel companion are solely responsible for obtaining valid travel documents such as passports, visas, etc. and any other documents necessary for travel. Winner and travel companion may be required to present a valid major credit card to hotel upon check in. If winner elects to travel with no travelling companion, no additional compensation will be awarded. Travel restrictions, conditions and limitations may apply. Sponsor will not replace any lost, mutilated, or stolen tickets, travel vouchers or certificates. </p>
<p><strong>INSTANT WIN PRIZES: </strong>There will be one thousand (1,000) instant win prizes (each an “Instant Win Prize” and collectively, the “Instant Win Prizes”) available to be won each consisting of: one (1) promotional code valid for two (2) movie tickets purchased on www.cineplex.com with a maximum combined value of $25.98. Promotional  code expires August 31, 2015.</p>
<p>Both movie tickets must be redeemed on the same day and for the same performance only. Promotional code cannot be redeemed in theatre, and is only valid for films at Cineplex Inc. theatres. A surcharge may apply for IMAX®, UltraAVX®, UltraAVX®3D, VIP Room admission, Digital 3D presentation, D-BOX Seating, or a Reserved seating performance if the combined value exceeds $25.98. Cannot be combined with any other promotion, coupon, voucher, or special discount offer. Not valid for non-feature film screenings, or Front Row Centre Events. Movie tickets are not eligible to earn SCENE points. Movie tickets are non-refundable. Offers cannot be redeemable for cash and are not valid for re-sale. No time extensions allowed. ARV of each Instant Win Prize: $25.98. The number of Instant Win Prizes available to be won will diminish throughout the Contest Period. LIMIT of one (1) Instant Win Prize per person during the Contest Period. Instant Win Prizes are subject to terms and conditions contained on the movie tickets. </p>
<p>To redeem Instant Win Prize, winner will receive instructions to visit www.cineplex.com and click on Showtimes link to find and select the show time for desired film at their desired theatre, and enter promotional code on the Details page during checkout. DO NOT present code at box office. See <a href="http://www.cineplex.com/">Cineplex.com</a> for details. Internet access and printer or smartphone required. </p>
<p><strong>FOR ALL PRIZES: </strong>The approximate retail value of each Prize is that stated by the supplier of the Prize as being in effect thirty (30) days prior to the launching of this Contest.</p>
<p><strong>9) LIMITATION OF LIABILITY: </strong>By entering the Contest, entrant, and his/her parent/legal guardian, if entrant is under the age of majority in his/her province/territory of residence, accepts and agrees to these Official Contest Rules and the decisions of Sponsor, which shall be final in all matters. By accepting a Prize, winner, and his/her parent/legal guardian, if entrant is under the age of majority in his/her province/territory of residence, agrees to hold Promotion Parties, and each of their respective parent companies, subsidiaries, affiliates and/or related companies and each of their directors, officers, employees, suppliers, agents, sponsors, administrators, licensees, representatives, advertising, media buying and promotional agencies (collectively, the “Releasees”) harmless from and against any and all claims and liability arising out of acceptance, possession use or misuse of Prize or participation in the Contest. Releasees are not responsible for lost or late Entries or Releases, or for any typographical, or other error in the printing of the offer, administration of the Contest, or announcement of a Prize, or for technical, hardware, or software malfunctions, computer virus, bugs, tampering, unauthorized intervention, fraud, lost or unavailable network connections, or failed, incorrect, inaccurate, incomplete, garbled, or delayed electronic communications whether caused by the sender or by any of the equipment or programming associated with or utilized in this Contest, or by any human error which may occur in the processing of the Entries in this Contest, or any other cause beyond the reasonable control of Sponsor that interferes with the proper conduct of the Contest as contemplated by these Official Contest Rules. If, in the Sponsor’s opinion, there is any suspected or actual evidence of tampering with any portion of the Contest, or if technical difficulties or any other factor including accident, printing, administrative, or any error of any kind compromises the integrity, administration, or conduct of the Contest, the Sponsor reserves the right, with consent of the Régie des alcools, des courses et des jeux, in respect of Quebec residents, to modify, cancel, or suspend this Contest without prior notice or obligation. Any attempt to deliberately damage the Contest Website or any website or to undermine the legitimate operation of this Contest is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law, including criminal prosecution. </p>
<p>Sponsor reserves the right, at its sole discretion, to disqualify a person if he/she enters the Contest or tries to do so by any means contrary to these Official Contest Rules or which would be unfair to other entrants or where Contest Entries are generated by any mechanical or automated means. Entry materials that have been tampered with, reproduced, falsified, or altered are void. </p>
<p>In the event a dispute arises as to the identity of a selected entrant, Entries received shall be deemed to be submitted by the authorized account holder of the e-mail address from which the entrant submitted on the Entry Form. For the purpose of this Contest, “authorized account holder” of an e-mail address is defined as the natural person who is assigned to an e-mail address by an Internet access provider, on-line service provider, or other organization responsible for assigning e-mail addresses for the domain associated with the submitted e-mail address. Each selected entrant may be required to provide the Sponsor with proof that the selected entrant is the authorized account holder of the e-mail address associated with the winning Entry.</p>
<p>Currency is in Canadian dollars unless otherwise stipulated. Prizes must be accepted “as is” and may not be exchanged for an amount of money, bartered, sold, raffled, substituted, or transferred, and is non-refundable. In the event the a stated Prize is unavailable due to reasons beyond the control of Sponsor, a prize of equivalent nature and value will be available to be won, or Sponsor, in its sole option, may award the winner the equivalent monetary value in lieu of Prize. Prizes will be awarded only to confirmed winners.</p>
<p>In no event shall Sponsor be held to award more prizes than what is mentioned in these Official Contest Rules or to award prizes otherwise than in compliance with these Official Contest Rules.</p>
<p><strong>10) DISCREPANCIES:</strong> In the event of any discrepancy or inconsistency between the Official Contest Rules and/or any other Contest-related material; including, but not limited to advertising, point-of-sale, and/or product packaging, as applicable, the Official Contest Rules shall prevail, govern and control. In the event of any discrepancy or inconsistency between the English language version and the French language version of the Official Contest Rules or of any abridged statement of the Official Contest Rules, the English version shall prevail, govern and control.</p>
<p><strong>11) CONDITIONS OF ENTRY:</strong> Acceptance of a Prize constitutes permission for the Sponsor, and their agencies to use winners’ names, cities and provinces/territories of residence and likenesses for purposes of advertising and publicity in any and all media now or hereafter known throughout the world in perpetuity, without further compensation, notification, or permission, unless prohibited by law. </p>
<p>All information requested by and supplied by each entrant must be truthful, accurate, and in no way misleading. Sponsor reserves the right to disqualify any entrant from the Contest in its discretion, should the entrant at any stage supply untruthful, inaccurate, or misleading details and/or information or should the entrant be ineligible for the Contest pursuant to these Official Contest Rules.</p>
<p><strong>12) LAW. </strong>These are the Official Contest Rules. The Contest is subject to applicable federal, provincial and municipal laws. The Official Contest Rules are subject to change without notice (with the consent of the Régie des alcools, des courses et des jeux, in respect of Quebec residents) in order to comply with any applicable federal, provincial and municipal laws or the policy of any other entity having jurisdiction over the Sponsor. All issues and questions concerning the construction, validity, interpretation and enforceability of the Official Contest Rules or the rights and obligations as between the entrant and the Sponsor in connection with the Contest shall be governed by and construed in accordance with the laws of the province of Ontario including procedural provisions without giving effect to any choice of law or conflict of law rules or provisions that would cause the application of any other jurisdiction’s laws.</p>
<p><strong>13) PROVINCE OF QUEBEC:</strong> Any litigation respecting the conduct or organization of the Contest may be submitted to the Régie des alcools, des courses et des jeux for a ruling. Any litigation respecting the awarding of a Prize in the Contest may be submitted to the Régie only for the purpose of helping the parties reach a fair settlement.</p>
<p><strong>14) FACEBOOK: </strong>This Contest is in no way sponsored, endorsed, administered by, or associated with Facebook.By entering, you completely release Facebook from all liability in connection with this Contest.  You are providing your information to the Sponsor and not to Facebook.  Any questions or concerns regarding the Contest should be directed to Sponsor and not to Facebook.</p>
<p><strong>15)  PRIVACY: </strong>Sponsor respects entrants’ right to privacy.  Entrants’ names, addresses, telephone numbers, and e-mail addresses are gathered for the purpose of administering this Contest and conducting publicity about this Contest.  By entering the Contest, entrants consent to Sponsor’s collection, use, and disclosure of their personal information for these purposes.  For a copy of Sponsor’s on-line Privacy Promise, please visit snackworks.ca.<br>
</p>
<p>&nbsp;</p>
<p></p>
';

//Prize page
$lang['grand_prize'] = 'Grand Prize';
$lang['grand_prize_text'] = 'A trip for two (2) to Hollywood, California. Trip includes roundtrip coach-class air transportation for two (2) from the international airport closest to  winner\'s residence (as determined by Sponsor in its sole discretion) to Hollywood, California;  two (2) consecutive nights\' accommodations at a hotel determined by Sponsor at its sole discretion (based on one (1) standard room with double occupancy); return ground transportation between airport LAX and hotel; and $1,000 CAD spending money for the winner. Approximate Retail Value ("ARV"): $7,500 CAD based on sample Toronto departure.';
$lang['instant_prize_text'] = 'Instant Win Prize';
$lang['instant_prize_explain'] = 'One thousand (1000) promotional codes for two (2) Cineplex Movie Tickets.  Limit of one (1) Instant Win Prize per person during the Contest Period. Approximate retail value: $25.98 each pair of tickets';

//Landing page
$lang['like_title'] = 'Like our page to ENTER';
$lang['like_text'] = '<span>You could</span>WIN<em>*</em> free Movie Tickets';
$lang['trip_to_hollywood'] = 'plus a VIP Trip for 2 to Hollywood';

//Error page
$lang['daily_limit_text'] = 'We\'re sorry but you have reached your daily participations limit. Please try again tomorrow.';
$lang['technical_error_text'] = 'We\'re sorry, there has been a technical error with the system<br /> and your information has not been sent. <br /> Please click the back button and try again.';

//Liked page
$lang['rules_link'] = '*See <a href="/home/rules" target="_blank">Official Rules</a> for details.';

//Congratulations email
$lang['congratulations_text'] = 'We\'re excited to announce that you\'re a winner of two (2) Cineplex movie passes,<br>
         which has a retail value of approximately $25.98! Lucky you!<br>';
$lang['prize_redeem_explain'] = 'To redeem the Cineplex movie passes please visit Cineplex.com and click on Showtimes to find and select the show time for the desired film, and enter pin code above on the details page. DO NOT present pin code at box office. See <a href="http://www.cineplex.com/">Cineplex.com</a> for details. Internet access and printer or Smartphone required.';
$lang['pin_expiration_explain'] = 'Pin code expires August 31, 2015 (Offer valid in Canada only). Additional conditions & restrictions apply.';
$lang['email_rules_link'] = 'See <a href="https://winwithdentyne.ca/home/rules">Official Rules</a> for details.';
$lang['thank_you'] = 'Thank you,<br>DENTYNE';
$lang['congratulations_subject'] = 'Dentyne Contest - Congratulations!';
$lang['contest_title'] = 'Dentyne Contest';
$lang['email_header_image'] = 'css/images/email_header.jpg';
$lang['pin_code_header'] = 'PIN CODE';
$lang['email_congratulations'] = 'Congratulations, ';

//Faq page
$lang['faq_text'] = '<h1>Frequently Asked Questions</h1>
<p>While the Official Rules outline the complete details of this promotion, your question may already be answered below. </p>
<p>See the Official Rules for complete details.</p>
<p><strong>What are the prizes for this promotion?</strong><br>
See the Official Rules for complete details.</p>
<p><strong>How many prizes could I win?</strong><br>
One Instant Win Prize and one Grand Prize per  person. </p>
<p><strong>I won an Instant Win Prize, but never received  an email with more details. What should I do?</strong><br>
Check your junk mail to make sure you have not  received the email. To make sure you get emails, add <a href="mailto:contest@winwithdentyne.ca">contest@winwithdentyne.ca</a> to your address book and email us to let us know you didn’t receive your win email. </p>
<p><strong>I won an Instant Win  Prize, but deleted or lost the email with the prizing details. What should I  do?</strong><br>
Check your deleted  box for the email. If it is still missing please contact <a href="mailto:contest@winwithdentyne.ca">contest@winwithdentyne.ca</a> with your  &nbsp;details. If you are a confirmed winner you will receive another  notification with prizing details.</p>
<p><strong>How will I know if I\'m the Grand Prize winner?</strong><br>
The Grand Prize draw will be held on August 27,  2014 at 12:00 p.m. (noon) Eastern Time  (&ldquo;ET&rdquo;). The potential  winner will be notified within forty-eight (48) hours of the draw via email  and/or telephone at the contact information provided at time of entry. </p>
<p><strong>I got a message that I\'ve already entered today,  but I haven\'t. What should I do?</strong><br>
First, make sure you are visiting <a target="_blank" href="http://www.WinWithDentyne.ca">www.WinWithDentyne.ca</a> or <a target="_blank" href="http://www.facebook.com/DentyneCanada">www.facebook.com/DentyneCanada</a>directly rather than a bookmarked page in your  browser. You may also need to clear your Temporary Internet Files. Just click  \'Tools\' on the menu bar at the top of your browser and selecting \'Internet  Options\' from the drop down menu. Choose the \'Delete Files\' button and delete  all of your Temporary Internet Files. This should help.  If you are still having issues, email us at <a href="mailto:contest@winwithdentyne.ca">contest@winwithdentyne.ca</a> </p>
<p><strong>I think I\'m having technical issues. What should I do?</strong><br>
Sometimes it\'s a simple thing like updating your browser. Check to be  sure that you have a modern version of Chrome, Internet Explorer, Firefox, or  Safari. Also, you may want to be sure you have the latest Flash player (it\'s  free!) and check your browser preferences section to be sure JavaScript is  enabled. </p>
<p><strong>I have recently moved or updated my email address. Can I change my entry  information for this promotion?</strong><br>
  We apologize for any inconvenience, we are unable to change information you provided on the entry form during the promotion unless you are awarded a prize. If you do win a prize during the promotional period, please contact
<a href="mailto:contest@winwithdentyne.ca">contest@winwithdentyne.ca</a> and provide your updated information.</p>
<p><strong>Why can\'t I use my P.O. Box address on the entry form?</strong><br>
We need your home address to verify eligibility and  for prize fulfillment if you win.</p>
<p><strong>I saw a message that I was an instant winner on the site and I\'m sure I  inputted the correct answer to the mathematical question, but the site said I  took too long to answer and did not win a prize.</strong><br>
As noted on the skill testing question page, there is a five (5) minute  time limit to submit your answer to the skill-testing question.  Unfortunately, taking longer than five (5)  minutes is cause for disqualification; the user is not eligible for the instant  win prize. See Official Contest Rules for details.</p>
<p><strong>I won a prize but I am a minor. What do I do?</strong><br>
  As per the Official Contest Rules, if you are under the age of majority  in the province or territory in which you reside, the signature of your parent  or legal guardian will be required on the Contest Release form in order for us  to award the prize. <br>
  <br>
</p>
';
//Didnt win page
$lang['didnt_win_test'] = 'You didn’t win an Instant Win Prize but are still eligible for the Grand Prize draw. <br />

							 	You can enter once per day so remember to come back tomorrow!';

