<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_modify_upc_code_table extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
ALTER TABLE `upc_code` DROP `already_used`, DROP `user_id`, DROP `date_used`;
SQL;
	$this->db->query($sql);

	$sql = <<<SQL
ALTER TABLE  `upc_code` ADD  `description` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER  `code`;
SQL;

	$this->db->query($sql);

	$sql = <<<SQL
INSERT INTO `upc_code` (`id`, `code`, `description`) VALUES (NULL, '5770000110', 'DENTYNE ICE MID MINT BTLS 60PC '), (NULL, '5770001052', 'DENTYNE ICE AVALANCHE BTLS 60PC '), (NULL, '5770021806', 'DENTYNE FIRE CINNAMON BTLS 60PC '), (NULL, '5770021807', 'DENTYNE ICE SPEARMINT BTLS 60PC '), (NULL, '5770022657', 'DENTYNE ICE INTENSE BTLS 60PC '), (NULL, '5770024390', 'DENTYNE ICE PPRMINT BTLS 60PC ');
SQL;

	$this->db->query($sql);
	}
}



$sql = "ALTER TABLE `upc_code` DROP `already_used`, DROP `user_id`, DROP `date_used`;";