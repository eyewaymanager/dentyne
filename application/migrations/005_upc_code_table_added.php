<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_upc_code_table_added extends CI_Migration {

	public function up(){
		
		$sql = "CREATE TABLE `upc_code` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`code` varchar(512) NOT NULL,
				`already_used` tinyint(1) NOT NULL,
				`user_id` int(4) NOT NULL,
				`date_used` timestamp NOT NULL,
				PRIMARY KEY (`id`),
				FULLTEXT KEY `code` (`code`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);
	}

	public function down(){

	}
}