<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_available_from_column_added extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
ALTER TABLE  `pin_codes` ADD  `available_from` DATETIME COLLATE utf8_general_ci NOT NULL AFTER  `won`;
SQL;

	$this->db->query($sql);
	}
}