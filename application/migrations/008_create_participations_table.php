<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_create_participations_table extends CI_Migration {

	public function up(){
		
		$sql = "CREATE TABLE `participations` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`participant_id` varchar(40) NOT NULL,
				`upc_code_id` varchar(40) NOT NULL,
				`date_attempted` timestamp NOT NULL,
				`winner` tinyint(4) NOT NULL,
				PRIMARY KEY (`id`)
			 	)ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);
	}

	public function down(){

	}
}