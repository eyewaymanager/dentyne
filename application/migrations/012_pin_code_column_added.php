<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_pin_code_column_added extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
ALTER TABLE  `participations` ADD  `pin_code` INT( 11 ) NULL AFTER  `winner`
SQL;

	$this->db->query($sql);
	}
}