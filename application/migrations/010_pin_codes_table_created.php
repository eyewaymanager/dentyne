<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_pin_codes_table_created extends CI_Migration {

	public function up(){
		
		$sql = "CREATE TABLE `pin_codes` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`pin_code` varchar(255) NOT NULL,
				`won` tinyint(4) NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			 	)ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);
	}

	public function down(){

	}
}