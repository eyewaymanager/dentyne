<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_captcha_table extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
CREATE TABLE captcha (
 captcha_id bigint(13) unsigned NOT NULL auto_increment,
 captcha_time int(10) unsigned NOT NULL,
 ip_address varchar(16) default '0' NOT NULL,
 word varchar(20) NOT NULL,
 PRIMARY KEY `captcha_id` (`captcha_id`),
 KEY `word` (`word`)
);
SQL;

	$this->db->query($sql);
	}
}