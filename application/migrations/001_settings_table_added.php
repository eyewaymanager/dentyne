<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_settings_table_added extends CI_Migration {

	public function up(){

		$sql = "CREATE TABLE `settings` (
 `id` tinyint(4) NOT NULL,				
 `title` varchar(512) NULL,
 `description` varchar(512) NULL,
 `logo` varchar(512) NULL,
 `keywords` varchar(512) NULL,
 `copyright` varchar(512) NULL) 
ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);

		$sql = <<<SQL
--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `description`, `logo`, `keywords`, `copyright`) VALUES
(1, NULL, NULL, NULL, NULL, NULL);
SQL;

$this->db->query($sql);
	}
}