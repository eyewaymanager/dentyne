<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_modify_facebook_id_column extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
ALTER TABLE `participants` CHANGE `facebook_id` `facebook_id` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL
SQL;

	$this->db->query($sql);
	}
}