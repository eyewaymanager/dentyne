<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_language_column_added extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
ALTER TABLE  `participants` ADD  `language` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `facebook_id`
SQL;

	$this->db->query($sql);
	}
}