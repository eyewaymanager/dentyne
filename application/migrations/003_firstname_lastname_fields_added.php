<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_firstname_lastname_fields_added extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
ALTER TABLE `users`  ADD `first_name` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `password`,  ADD `last_name` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `first_name`;
SQL;

	$this->db->query($sql);
	}
}