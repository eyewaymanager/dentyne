<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_participants_table_created extends CI_Migration {

	public function up(){
		
		$sql = "CREATE TABLE `participants` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`first_name` varchar(512) NOT NULL,
				`last_name` varchar(512) NOT NULL,
				`facebook_id` varchar(512) NOT NULL,
				`postal_code` varchar(512) NOT NULL,
				`email` varchar(512) NOT NULL,
				`telephone` varchar(512) NOT NULL,
				`birthday` varchar(512) NOT NULL,
				`newsletter` tinyint(4) NOT NULL,
				`date_created` timestamp NOT NULL,
				PRIMARY KEY (`id`)
			 	)ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);
	}

	public function down(){

	}
}