<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_articles_tables_added extends CI_Migration {

	public function up(){
		
		$sql = "CREATE TABLE `articles` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `title` varchar(512) NOT NULL,
 `description` text NOT NULL,
 `content` text NOT NULL,
 `tags` varchar(512) NOT NULL,
 `thumb_url` varchar(512) NOT NULL,
 `banner_url` varchar(512) NOT NULL,
 `video_url` varchar(512) NOT NULL,
 `type` enum('article','video') NOT NULL,
 `featured_position` tinyint(1) NOT NULL DEFAULT '0',
 `category_id` int(3) NOT NULL,
 `published` tinyint(1) NOT NULL,
 `url` varchar(1024) NOT NULL,
 `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `views` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 FULLTEXT KEY `title` (`title`,`description`,`content`,`tags`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);

		$sql = "CREATE TABLE `categories` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `name` varchar(512) NOT NULL,
 `short_name` varchar(512) NOT NULL,
 `background` varchar(64) NOT NULL,
 `font` varchar(64) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);

		$sql = "CREATE TABLE `related` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `article_id` int(11) NOT NULL,
 `related_to_article_id` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8";

		$this->db->query($sql);

	}

	public function down(){

	}
}