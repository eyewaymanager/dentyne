<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_session_table_added extends CI_Migration {

	public function up()
	{
		$sql = <<<SQL
CREATE  TABLE IF NOT EXISTS `sessions` (
  `sess_id` VARCHAR(40) NOT NULL ,
  `sess_ip` VARCHAR(16) NOT NULL ,
  `sess_user_agent` VARCHAR(120) NOT NULL ,
  `sess_date_activity` DATETIME NOT NULL ,
  `sess_date_expire` DATETIME NOT NULL ,
  `sess_date_expire_id` DATETIME NOT NULL ,
  `sess_data` TEXT NOT NULL ,
  PRIMARY KEY (`sess_id`) )
ENGINE = InnoDB
SQL;

	$this->db->query($sql);
	}
}