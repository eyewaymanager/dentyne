<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/
$config['mailtype'] = 'html';
$config['newline'] = "\r\n";
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;
$config['protocol'] = 'mail';



/* End of file email.php */
/* Location: ./application/config/email.php */