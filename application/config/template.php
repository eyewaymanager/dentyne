<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'default';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['default']['template'] = 'front_template';
$template['default']['regions'] = array(
   'header',
   'menu',
   'content',
   'footer',
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

$template['admin_template']['template'] = 'admin_template';
$template['admin_template']['regions'] = array(
   'header',
   'menu',
   'content',
   'footer',
);
$template['admin_template']['parser'] = 'parser';
$template['admin_template']['parser_method'] = 'parse';
$template['admin_template']['parse_template'] = FALSE;

$template['coming_soon_template']['template'] = 'coming_soon_template';
$template['coming_soon_template']['regions'] = array(
   
   'content',
   
);
$template['coming_soon_template']['parser'] = 'parser';
$template['coming_soon_template']['parser_method'] = 'parse';
$template['coming_soon_template']['parse_template'] = FALSE;


/*
|--------------------------------------------------------------------------
| Assets configuration
|--------------------------------------------------------------------------
*/

$assets['js'] = array(
	'prefix' 		=> "<script type='text/javascript' src='",
	'postfix' 		=> "'></script>",
	'server_path' 	=> FCPATH . "js/",
	'web_path' 		=> "js/",
	'server_cache'	=> FCPATH . "js/cache/",
	'cache'			=> false,
	'web_cache'		=> "js/cache/",
	'combine'		=> FALSE,
	'ext' 			=> '.js',
	'hooks' 		=> array(
			'pre_process' => array(
			)
	)
);

$assets['css'] = array(
	'prefix' 		=> '<link rel="stylesheet" type="text/css" href="',
	'postfix' 		=> '" />',
	'server_path' 	=> FCPATH . "css/",
	'web_path' 		=> "css/",
	'server_cache'	=> FCPATH . "css/cache/",
	'cache'			=> false,
	'web_cache'		=> "css/cache/",
	'combine'		=> FALSE,
	'ext' 			=> '.css',
	'hooks' 		=> array(
			'pipeline' => array(
					array( 'function' => 'less_parser', 'filename' => 'less_parser.php', 'filepath' => 'hooks/assets/css'),
			)
	)
);
/* End of file template.php */
/* Location: ./system/application/config/template.php */