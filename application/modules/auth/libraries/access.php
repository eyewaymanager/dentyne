<?php

class Access {

	protected $ci;
	protected $user_roles;

	// the users table
	protected $users_table = 'users';

	// use roles and/or groups
	protected $use_roles = TRUE;
	protected $use_groups = FALSE;

	// db config
	protected $user_roles_table = 'roles';
	protected $user_groups_table = 'groups';
	protected $user_group_id_field = 'group_id';
	protected $user_id_field_in_roles_table = 'user_id';
	protected $roles_to_users_table = 'user_roles';

	// session variables
	protected $user_id_session_key = 'user_id';
	protected $user_roles_session_key = 'roles';
	protected $user_group_session_key = 'group';
	protected $user_group_id_session_key = 'group_id';

	public function __construct()
	{
		$this->ci = get_instance();

		log_message('debug', 'Access class initialized');
	}

	/**
	 * Checks to see if a user is in a user group
	 *
	 * @return bool
	 **/
	public function in_group($required_group, $user_id = FALSE)
	{
		// check to see if the group is an integer (id) or string
		if (is_int($required_group))
		{
			$user_group = $this->ci->session->userdata($this->user_group_id_session_key);
		}
		else
		{
			$user_group = $this->ci->session->userdata($this->user_group_session_key);
		}

		// see if the user is in the group
		if ($required_group == $user_group)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Checks to see if a user has the specified role(s)
	 *
	 * Accepts a string as a single role or an array or required roles
	 *
	 * @return bool
	 **/
	public function has_role($required_roles, $user_id = FALSE)
	{
		// grab the user roles form the session
		$this->user_roles = $this->ci->session->userdata($this->user_roles_session_key);

		// if an array of required roles is passed...
		if (is_array($required_roles))
		{
			// check to see if user has all required roles
			foreach ($required_roles as $role)
			{
				if ( ! $this->check_user_has_required_role($role))
				{
					return FALSE;
				}
			}
		}
		elseif (is_string($required_roles))
		{
			if ( ! $this->check_user_has_required_role($required_roles))
			{
				return FALSE;
			}
		}
		else // i don't know what you're doing...
		{
			show_error('You must pass a string or an array to the has_role method.');
		}

		return TRUE;
	}

	/**
	 * A convenience method
	 *
	 * @return void
	 **/
	public function has_roles($required_roles)
	{
		return $this->has_role($required_roles);
	}

	/**
	 * Checks to see if the user has one of the required roles.
	 *
	 * This is what allows for the hierarchy or roles.
	 *
	 * @return bool
	 **/
	private function check_user_has_required_role($required_roles)
	{
		// turn the roles hierarchy into an array
		$roles = explode(':', $required_roles);

		// check if the user has the required roles
		foreach ($roles as $role)
		{
			// if they have one of the required roles, they should be all good
			if (@in_array($role, $this->user_roles))
			{
				return TRUE;
			}
		}

		return FALSE;
	}

	/**
	 * Sets up the user privileges in the session.
	 *
	 * This method should usually be called upon a successful login attempt.
	 *
	 * @return null
	 **/
	public function setup_user_privileges()
	{
		$user_id = $this->ci->session->userdata($this->user_id_session_key);

		if ($this->use_roles)
		{
			// create the user roles table if it doesn't exist
			if ( ! $this->ci->db->table_exists($this->user_roles_table))
			{
				show_error('Access.php:153 - Missing required tables');
			}

			// pull the roles from the database
			$user_roles = $this->ci->db
				->select($this->user_roles_table.'.name AS name')
				->where($this->user_id_field_in_roles_table, $user_id)
				->join($this->user_roles_table, $this->user_roles_table .'.id = '. $this->roles_to_users_table .'.role_id')
				->get($this->roles_to_users_table)
				->result();

			$user_privileges = array();
			foreach ($user_roles as $role)
			{
				array_push($user_privileges, $role->name);
			}

			if ($user_roles)
			{
				// set the roles in the session
				$this->ci->session->set_userdata($this->user_roles_session_key, $user_privileges);
			}
		}

		if ($this->use_groups)
		{
			// setup the user group in the session
			$user = $this->ci->db
				->select($this->users_table .'.*,'. $this->user_groups_table .'.name AS '. $this->user_group_session_key .','. $this->user_groups_table .'.id AS '. $this->user_group_id_session_key)
				->where($this->users_table.'.id', $user_id)
				->join($this->user_groups_table, $this->user_groups_table.'.id = '.$this->users_table.'.'.$this->user_group_id_field)
				->limit(1)
				->get($this->users_table)
				->row();

			if ($user)
			{
				$this->ci->session->set_userdata($this->user_group_session_key, $user->{$this->user_group_session_key});
				$this->ci->session->set_userdata($this->user_group_id_session_key, $user->{$this->user_group_id_session_key});
			}
		}
	}
}

