<?php
$login = array(
	'name'	=> 'login',
	'id'	=> '',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>
<div class="auth-page-container">
<div class="modal-header">
        <h3>Forgot Password</h3>
    </div>
<div class="forgot_password">
<?php echo form_open($this->uri->uri_string()); ?>
<table>
	<tr>
		<td><h4>Need a new account password?</h4>
			<p>Enter you email address and click Continue</p>
		</td>
	</tr>	
	<tr>
		<td>
			<?php echo form_label($login_label, $login['id']); ?>
			<?php echo form_input($login); ?>
		</td>
		<td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>
	</tr>
	<tr>
		<td><button class="action-button dialog-button" type="submit">Continue</button></td>
	</tr>
</table>

<?php echo form_close(); ?>
</div>
</div>
