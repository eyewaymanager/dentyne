<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo $this->config->item('facebook_id','tank_auth'); ?>', // App ID
      channelUrl : '<?php echo site_url(); ?>fb_channel.php', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

	// Additional initialization code here
	/*FB.login(function(response) {
		if (response.authResponse) {
			window.top.location = "<?php echo site_url('auth/facebook_registration');?>";
		}
  });
	FB.Event.subscribe('auth.authResponseChange', function(response) {
		 if (response.status === 'connected') {
    	  // the user is logged in and has authenticated your
    	  // app, and response.authResponse supplies
  		  // the user's ID, a valid access token, a signed
  		  // request, and the time the access token 
  		  // and signed request each expire
	  		 window.top.location = "<?php echo site_url('auth/facebook_registration');?>";
  		} else if (response.status === 'not_authorized') {
  		  // the user is logged in to Facebook, 
  		  // but has not authenticated your app
  		} else {
			// the user isn't logged in to Facebook.
			
  		}

  });*/
	FB.Event.subscribe("auth.login", function() {window.location = '/auth/facebook_registration'});
	FB.Event.subscribe("auth.logout", function() {window.location = '/auth/logout'});
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
  }(document));

	
</script>
