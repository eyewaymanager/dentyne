<?php

$first_name = array(
	'name'	=> 'first_name',
	'id'	=> 'first_name',
	'value'	=> set_value('first_name'),
	'label' => 'First Name',
	'class' => '',
	'error' => form_error('first_name'),
);
$last_name = array(
	'name'	=> 'last_name',
	'id'	=> 'last_name',
	'value'	=> set_value('last_name'),
	'label' => 'Last Name',
	'class' => '',
	'error' => form_error('last_name'),
);
$company_name = array(
	'name'	=> 'company_name',
	'id'	=> 'company_name',
	'value'	=> set_value('company_name'),
	'label' => 'Company Name',
	'class' => '',
	'error' => form_error('company_name'),
);
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'label' => 'Username',
		'class' => '',
		'error' => form_error('username'),
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'label' => 'Email',
	'class' => '',
	'error' => form_error('email'),
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'label' => 'Password',
	'class' => '',
	'error' => form_error('password'),
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'label' => 'Confirm Password',
	'class' => '',
	'error' => form_error('confirm_password'),
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'value' => '',
	'label' => 'Captcha',
	'class' => '',
	'error' => form_error('captcha'),
);
$submit = array(
	'label' => 'Let me in',
	'class' => '',
);
?>

<h1> Provider Registration </h1>

<?php if($this->config->item('login_by_facebook','tank_auth')){ ?>
<!--	<h2> Register with Facebook </h2>
	<?php $this->load->view('auth/facebook_sdk'); ?>
	<?php $this->load->view('auth/facebook_login_button'); ?>
	<hr /> -->
<?php } ?>

<?php echo form_open($this->uri->uri_string()); ?>

	<?php $this->element->load('beside_label_input_medium', $first_name);?>

	<?php $this->element->load('beside_label_input_medium', $last_name);?>

	<?php $this->element->load('beside_label_input_medium', $company_name);?>

	<?php if($use_username){  
		$this->element->load('beside_label_input_medium', $username);
	} ?>

	<?php $this->element->load('beside_label_input_medium', $email);?>

	<?php $this->element->load('beside_label_password_medium', $password);?>
	
	<?php $this->element->load('beside_label_password_medium', $confirm_password);?>

	
	<?php /*$this->element->load('before_label_checkbox_medium',
		array( 'name'=> $remember['name'] , 'value' => '1' , 'label' => 'Accept Terms'  , 'class' => '' , 'checked' => $remember['checked'] )
	);*/ ?>	

<?php /*
	<?php if ($captcha_registration) {
		if ($use_recaptcha) { ?>
	<tr>
		<td colspan="2">
			<div id="recaptcha_image"></div>
		</td>
		<td>
			<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="recaptcha_only_if_image">Enter the words above</div>
			<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
		</td>
		<td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
		<td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></td>
		<?php echo $recaptcha_html; ?>
	</tr>
	<?php } else { ?>
	<tr>
		<td colspan="3">
			<p>Enter the code exactly as it appears:</p>
			<?php echo $captcha_html; ?>
		</td>
	</tr>
	<tr>
		<td><?php echo form_label('Confirmation Code', $captcha['id']); ?></td>
		<td><?php echo form_input($captcha); ?></td>
		<td style="color: red;"><?php echo form_error($captcha['name']); ?></td>
	</tr>
	<?php }
	} ?>
*/ ?>

	<?php $this->element->load('submit_medium', $submit);?>

<?php echo form_close(); ?>
