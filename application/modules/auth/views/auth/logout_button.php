<?php
	$user = $this->facebook->getUser();
	if ($user) {
		try {
			$user_profile = $this->facebook->api('/me');

			//Display FB Button
			$this->load->view('auth/facebook_sdk');
			$this->load->view('auth/facebook_login_button');
				
		} catch (FacebookApiException $e) {
			//Display Logout Link
			echo "<a href='" . site_url('auth/logout') . "'>Logout</a>";	
		}
	} else {
		//Display Logout Link
		echo "<a href='" . site_url('auth/logout') . "'>Logout</a>";
	}
?>
