<?php echo form_close();
$captcha = array(
    'name'  => 'captcha',
    'id'    => 'captcha',
    'value' => '',
    'label' => 'Captcha',
    'class' => '',
    'error' => '',
); 
$this->config->load('countries');
?>
<?php echo form_open('auth/register'); ?>
<div class="auth-page-container" >
   

        <div class="modal-body">
            <div id="modal-content">
                <div class="modal-center">
				    <?php
                        $errors = $this->message->validation_errors();
					    foreach ($errors as $k => $v)	$this->message->set('error',$v) ;
                        $this->message->display();
                    ?>
                    <div style="width: 530px; text-align: center; ">
                    <p>Register</p>
                    
                    </div>
                    </br></br>

                    

                    <label for="first_name">FIRST NAME</label>
                    <input type="text" name="first_name" value="<?php echo set_value('first_name') ?>" /><span class="required">*</span></br>

                    <label for="last_name">LAST NAME</label>
                    <input type="text" name="last_name" value="<?php echo set_value('last_name') ?>" /><span class="required">*</span></br>

                    <label for="email">EMAIL</label>
                    <input type="text" name="email" value="<?php echo set_value('email') ?>" /><span class="required">*</span></br>

                    <label for="password">PASSWORD</label>
                    <input type="password" name="password" value="<?php echo set_value('password') ?>" /><span class="required">*</span></br>

                    <label for="confirm_password">CONFIRM PASSWORD</label>
                    <input type="password" name="confirm_password" /><span class="required">*</span></br>

                    </br></br>       
                                    
                    <p>Enter the code exactly as it appears:</p>
                    <?php echo $captcha_html; ?></br>
                                        
                    <div class="captcha-input"><?php echo form_input($captcha); ?></div>
                    
                                    
                       
                    </br>
                        <p class="terms">I have read and understand the Terms & Conditions and Privacy Policy<input type="checkbox" name="accept-terms" value="1" <?php echo set_value('accept-terms') ? 'checked' : '' ?> /><span class="required">*</span></p>
                </div>

            </div>

        </div>

    <div class="modal-footer">
        <a href="/auth/login" class="dialog-footer-link">Already Have an Account</a>
        <button class="dialog-button action-button">Register</button>
        <a href="/" class="dialog-button cancel-button">Cancel</a>
    </div>
</div>
<?php echo form_close(); ?>
