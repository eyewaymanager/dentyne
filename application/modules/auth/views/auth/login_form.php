
<?php echo form_open('/auth/login'); ?>
<div class="auth-page-container" >
    <div class="modal-header">
        <h3>LOGIN TO ADMIN</h3>
    </div>

        <div class="modal-body">
            <div id="modal-content">
                <div class="modal-center">
				    <?php
                        $errors = $this->message->validation_errors();
                        foreach ($errors as $k => $v)   $this->message->set('error',$v) ;
                        $this->message->display();
                    ?>
                    

                    <hr class='or'/>

                    <label for="login">EMAIL</label><input type="text" name="login" class="form-control" /></br>
                    <label for="password">PASSWORD</label><input type="password" name="password" class="form-control" />
                    <div style="width: 300px; margin-left: auto;"><a href="/auth/forgot_password">Forgot Password</a></div>
                </div>

            </div>

        </div>

    <div class="modal-footer">
        <!-- <a href="/auth/register" class="dialog-footer-link btn">Register</a> -->
        <button class="btn btn-primary">Login</button>
        <a href="/" class="btn btn-danger">Cancel</a>
    </div>
</div>
<?php echo form_close(); ?>
