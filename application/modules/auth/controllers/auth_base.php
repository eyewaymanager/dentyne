<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_base extends Admin_Controller
{
	//Used to override and control the redirect logic
	var $redirects = array(
		'index' => '/auth/login',
		'is_logged_in' => '/admin',
		'no_facebook_login' => '/auth/login',
		'logout' => '/',
		'login_success' => '/admin',
		'login_failed' => '/auth/login',
		'not_activated' => '/auth/send_again/',
		'registration_disabled' => '/auth/login',
		'registration_activation_link' => '/auth/login',
		'registration_success' => '/auth/login',
		'requires_authentication' => '/auth/login',
		'activation_success' => '/auth/login',
		'activation_fail' => '/auth/login',
		'reset_email' => '/auth/login',
	);

	//Used to override and control the view load logic
	var $view_load = array(
		'login' => 'auth/auth/login_form',
		'register' => 'auth/auth/register_form',
		'send_again' => 'auth/send_again_form',
		'forgot_password' => 'auth/auth/forgot_password_form',
		'reset_password' => 'auth/reset_password_form',
		'change_password' => 'auth/change_password_form',
		'change_email' => 'auth/change_email_form',
		'unregister' => 'auth/unregister_form',
	);


	function __construct()
	{
		parent::__construct();

		if(get_class($this) == "Auth_base"){
			show_404();
		}

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		//$this->load->library('security');
		$this->load->library('auth/tank_auth');
		$this->lang->load('auth/tank_auth');
	}

	function index()
	{
		redirect($this->redirects['index']);
	}

	/**
	 * Override methods for special hooks
	 */
	function _extra_registration($data){}
	function _extra_registration_validation(){}

	/**
	 * Process Facebook Registration
	 */

	function facebook_registration(){
		if( ! $this->config->item('login_by_facebook','tank_auth')){
			redirect($this->redirects['no_facebook_login']);
		}

		if( $this->tank_auth->is_logged_in()){
			redirect($this->redirects['is_logged_in']);
		}

		$user = $this->facebook->getUser();
		if ($user) {
			try {
				$user_profile = $this->facebook->api('/me');

				//Register User of Login
				$existing_user = $this->users->get_user_by_fb_id($user_profile['id']);
				if($existing_user != NULL){
					//Login
					$this->force_login($existing_user->email);
				} else {
					//Register
					if (!is_null($data = $this->tank_auth->create_user(
						$user_profile['email'],
						$user_profile['email'],
						md5('random_hash!@#$'), //TODO: use build in salt
						FALSE))) {									// success
							$this->_extra_facebook_registration($data);

						//Populate other fields
						$this->users->change_fb_id($data['user_id'], $user_profile['id']);

						$data['site_name'] = $this->config->item('website_name', 'tank_auth');

						if ($this->config->item('email_account_details', 'tank_auth')) {	// send "welcome" email
							$this->_send_email('welcome', $data['email'], $data);
						}
						unset($data['password']); // Clear password (just for any case)

						$this->force_login($data['email']);
					}
				}
			} catch (FacebookApiException $e) {
				return;
			}
		}
	}

	/**
	 *	Forces a login for a particular user without proper credentials
	 *
	 */
	private function force_login($login, $ignore_banned = FALSE){
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect($this->redirects['is_logged_in']);
		}

		if ($this->tank_auth->login(
			$login,
			'',
			FALSE,
			TRUE,
			TRUE,
			TRUE,
			$ignore_banned)) {								// success
				redirect($this->redirects['login_success']);

		} else {
			$errors = $this->tank_auth->get_error_message();
			if (isset($errors['banned'])) {								// banned user
				$this->message->set('error','You have been banned');
			} elseif (isset($errors['not_activated'])) {				// not activated user
				$this->message->set('error','User is not activated');
				redirect($this->redirects['not_activated']);

			} else {													// fail
				foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
			}
		}

		$this->message->set('error','Login Failed');
		redirect($this->redirects['login_failed']);
	}

	/**
	 * Login user on the site
	 *
	 * @return void
	 */
	function login()
	{
		$this->template->set_template('admin_template');
		$data['french_background'] = '';
		$data['facebook'] = '';
		$data['body_class'] = "page-form";
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect($this->redirects['is_logged_in']);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect($this->redirects['not_activated']);

		} else {
			$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
					$this->config->item('use_username', 'tank_auth'));
			$data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

			$this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('remember', 'Remember me', 'integer');

			// Get login for counting attempts to login
			if ($this->config->item('login_count_attempts', 'tank_auth') AND
					($login = $this->input->post('login'))) {
				$login = $this->security->xss_clean($login);
			} else {
				$login = '';
			}

			$data['use_recaptcha'] = $this->config->item('use_recaptcha', 'tank_auth');
			if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
				if ($data['use_recaptcha'])
					$this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
				else
					$this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
			}
			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->login(
						$this->form_validation->set_value('login'),
						$this->form_validation->set_value('password'),
						$this->form_validation->set_value('remember'),
						$data['login_by_username'],
                        $data['login_by_email'])) {								// success

                    $this->access->setup_user_privileges();
					redirect($this->redirects['login_success']);

				} else {
					$errors = $this->tank_auth->get_error_message();
					if (isset($errors['banned'])) {								// banned user
						$this->message->set('error','Your Account Is Locked. Please Contact <a href="mailto:support@mobileprwire.com">Support</a> For More Details.');
					} elseif (isset($errors['not_activated'])) {				// not activated user
						redirect($this->redirects['not_activated']);

					} else {													// fail
						foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
					}
				}
			}
			$data['show_captcha'] = FALSE;
			if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
				$data['show_captcha'] = TRUE;
				if ($data['use_recaptcha']) {
					$data['recaptcha_html'] = $this->_create_recaptcha();
				} else {
					$data['captcha_html'] = $this->_create_captcha();
				}
			}
			$this->template->write_view('content',$this->view_load['login'], $data);
		}

		$this->template->render();
	}

	/**
	 * Logout user
	 *
	 * @return void
	 */
	function logout()
	{
		$this->tank_auth->logout();
		$this->message->set('success','You have been logged out');
		redirect($this->redirects['logout']);
	}


	/**
	 * Register user on the site
	 *
	 * @return void
	 */
	function register()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in
            redirect($this->redirects['is_logged_in']);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect($this->redirects['not_activated']);

		} elseif (!$this->config->item('allow_registration', 'tank_auth')) {	// registration is off
			$this->message->set('error','Registration Disabled');
			redirect($this->redirects['registration_disabled']);

		} else {
			$use_username = $this->config->item('use_username', 'tank_auth');
			if ($use_username) {
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash');
			}
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');

			//Hook for validation override
			$this->_extra_registration_validation();

			$captcha_registration	= $this->config->item('captcha_registration', 'tank_auth');
			$use_recaptcha			= $this->config->item('use_recaptcha', 'tank_auth');
			if ($captcha_registration) {
				if ($use_recaptcha) {
					$this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
				} else {
					$this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
				}
			}
			$data['errors'] = array();

			$email_activation = $this->config->item('email_activation', 'tank_auth');

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->create_user(
						$use_username ? $this->form_validation->set_value('username') : '',
						$this->form_validation->set_value('email'),
						$this->form_validation->set_value('password'),
						$email_activation))) {									// success

						//Hook for adding extra registration fields
						$this->_extra_registration($data);


					$data['site_name'] = $this->config->item('website_name', 'tank_auth');

					if ($email_activation) {									// send "activate" email
						$data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

						$this->_send_email('activate', $data['email'], $data);

						unset($data['password']); // Clear password (just for any case)

						$this->message->set('success','Registration Complete. An email has been sent with instructions on activating your account.');
						redirect($this->redirects['registration_activation_link']);
					} else {
						if ($this->config->item('email_account_details', 'tank_auth')) {	// send "welcome" email

							$this->_send_email('welcome', $data['email'], $data);
						}
						unset($data['password']); // Clear password (just for any case)
						
						$this->_set_user_role($data['user_id']);
						$this->message->set('success','Registration Complete.  Login to gain access');
						redirect($this->redirects['registration_success']);
					}
				} else {
                    $errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
				}
            }

			if ($captcha_registration) {
				if ($use_recaptcha) {
					$data['recaptcha_html'] = $this->_create_recaptcha();
				} else {
					$data['captcha_html'] = $this->_create_captcha();
				}
			}
			$data['use_username'] = $use_username;
			$data['captcha_registration'] = $captcha_registration;
			$data['use_recaptcha'] = $use_recaptcha;
			$this->template->write_view('content',$this->view_load['register'], $data);
		}

		$this->template->render();
	}

	/**
	 * Send activation email again, to the same or new email address
	 *
	 * @return void
	 */
	function send_again()
	{
		if (!$this->tank_auth->is_logged_in(FALSE)) {							// not logged in or activated
			redirect($this->redirects['requires_authentication']);

		} else {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->change_email(
						$this->form_validation->set_value('email')))) {			// success

					$data['site_name']	= $this->config->item('website_name', 'tank_auth');
					$data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

					$this->_send_email('activate', $data['email'], $data);

					$this->message->set('success', 'Activation email sent.');

				} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
				}
			}
			$this->template->write_view('content',$this->view_load['send_again'], $data);
			$this->template->render();
		}
	}

	/**
	 * Activate user account.
	 * User is verified by user_id and authentication code in the URL.
	 * Can be called by clicking on link in mail.
	 *
	 * @return void
	 */
	function activate()
	{
		$user_id		= $this->uri->segment(3);
		$new_email_key	= $this->uri->segment(4);

		// Activate user
		if ($this->tank_auth->activate_user($user_id, $new_email_key)) {		// success
			$this->message->set('error','Activation Complete. Please Login');
			redirect($this->redirects['activation_success']);
		} else {																//fail
			$this->message->set('error','Activation fail');
			redirect($this->redirects['activation_fail']);
		}
	}

	/**
	 * Generate reset code (to change password) and send it to user
	 *
	 * @return void
	 */
	function forgot_password()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect($this->redirects['is_logged_in']);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect($this->redirects['not_activated']);

		} else {
			$this->form_validation->set_rules('login', 'Email or login', 'trim|required|xss_clean');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->forgot_password(
						$this->form_validation->set_value('login')))) {

					$data['site_name'] = $this->config->item('website_name', 'tank_auth');

					// Send email with password activation link
					$this->_send_email('forgot_password', $data['email'], $data);

					$this->message->set('success','Reset Password Email Sent');
				} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
				}
			}
			$this->template->write_view('content',$this->view_load['forgot_password'], $data);
		}
		$this->template->render();
	}

	/**
	 * Replace user password (forgotten) with a new one (set by user).
	 * User is verified by user_id and authentication code in the URL.
	 * Can be called by clicking on link in mail.
	 *
	 * @return void
	 */
	function reset_password()
	{
		$user_id		= $this->uri->segment(3);
		$new_pass_key	= $this->uri->segment(4);

		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

		$data['errors'] = array();

		if ($this->form_validation->run()) {								// validation ok
			if (!is_null($data = $this->tank_auth->reset_password(
					$user_id, $new_pass_key,
					$this->form_validation->set_value('new_password')))) {	// success

				$data['site_name'] = $this->config->item('website_name', 'tank_auth');

				// Send email with new password
				$this->_send_email('reset_password', $data['email'], $data);

				$this->message->set('succes','New Password Set');
			} else {														// fail
				$this->message->set('error','New Password was NOT Set');
			}
		} else {
			// Try to activate user by password key (if not activated yet)
			if ($this->config->item('email_activation', 'tank_auth')) {
				$this->tank_auth->activate_user($user_id, $new_pass_key, FALSE);
			}

			if (!$this->tank_auth->can_reset_password($user_id, $new_pass_key)) {
				$this->message->set('error','New Password was NOT Set');
			}
		}
		$this->template->write_view('content',$this->view_load['reset_password'], $data);
		$this->template->render();
	}

	/**
	 * Change user password
	 *
	 * @return void
	 */
	function change_password()
	{
		if (!$this->tank_auth->is_logged_in()) {								// not logged in or not activated
			redirect($this->redirects['requires_authentication']);

		} else {
			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->change_password(
						$this->form_validation->set_value('old_password'),
						$this->form_validation->set_value('new_password'))) {	// success
						$this->message->set('succes','New Password Set');

				} else {														// fail
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
				}
			}
			$this->template->write_view('content',$this->view_load['change_password'], $data);
		}
		$this->template->render();
	}

	/**
	 * Change user email
	 *
	 * @return void
	 */
	function change_email()
	{
		if (!$this->tank_auth->is_logged_in()) {								// not logged in or not activated
			redirect($this->redirects['requires_authentication']);

		} else {
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->set_new_email(
						$this->form_validation->set_value('email'),
						$this->form_validation->set_value('password')))) {			// success

					$data['site_name'] = $this->config->item('website_name', 'tank_auth');

					// Send email with new email address and its activation link
					$this->_send_email('change_email', $data['new_email'], $data);

					$this->message->set('succes','Email Verification Sent');

				} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
				}
			}
			$this->template->write_view('content',$this->view_load['change_email'], $data);
		}
		$this->template->render();
	}

	/**
	 * Replace user email with a new one.
	 * User is verified by user_id and authentication code in the URL.
	 * Can be called by clicking on link in mail.
	 *
	 * @return void
	 */
	function reset_email()
	{
		$user_id		= $this->uri->segment(3);
		$new_email_key	= $this->uri->segment(4);

		// Reset email
		if ($this->tank_auth->activate_new_email($user_id, $new_email_key)) {	// success
			$this->tank_auth->logout();
			$this->message->set('success','New Email Activated');

		} else {																// fail
			$this->message->set('error','New Email was NOT Activated');
		}

		redirect($this->redirects['reset_email']);
	}

	/**
	 * Delete user from the site (only when user is logged in)
	 *
	 * @return void
	 */
	function unregister()
	{
		if (!$this->tank_auth->is_logged_in()) {								// not logged in or not activated
			redirect($this->redirects['requires_authentication']);

		} else {
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->delete_user(
						$this->form_validation->set_value('password'))) {		// success
						$this->message->set('success','Your account has been deactivated');
				} else {														// fail
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$this->message->set('error',$this->lang->line($v)) ;
				}
			}
			$this->template->write_view('content',$this->view_load['unregister'], $data);
		}
		$this->template->render();
	}

	/**
	 * Send email message of given type (activate, forgot_password, etc.)
	 *
	 * @param	string
	 * @param	string
	 * @param	array
	 * @return	void
	 */
	function _send_email($type, $email, &$data)
	{
		$this->load->library('email');
		$this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->to($email);
		$this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
		$this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
		$this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
		$this->email->send();
	}

	/**
	 * Create CAPTCHA image to verify user as a human
	 *
	 * @return	string
	 */
	function _create_captcha()
	{
		$this->load->helper('captcha');

		$cap = create_captcha(array(
			'img_path'		=> './'.$this->config->item('captcha_path', 'tank_auth'),
			'img_url'		=> base_url().$this->config->item('captcha_path', 'tank_auth'),
			'font_path'		=> './'.$this->config->item('captcha_fonts_path', 'tank_auth'),
			'font_size'		=> $this->config->item('captcha_font_size', 'tank_auth'),
			'img_width'		=> $this->config->item('captcha_width', 'tank_auth'),
			'img_height'	=> $this->config->item('captcha_height', 'tank_auth'),
			'show_grid'		=> $this->config->item('captcha_grid', 'tank_auth'),
			'expiration'	=> $this->config->item('captcha_expire', 'tank_auth'),
		));

		// Save captcha params in session
		$this->session->set_flashdata(array(
				'captcha_word' => $cap['word'],
				'captcha_time' => $cap['time'],
		));

		return $cap['image'];
	}

	/**
	 * Callback function. Check if CAPTCHA test is passed.
	 *
	 * @param	string
	 * @return	bool
	 */
	function _check_captcha($code)
	{
		$time = $this->session->flashdata('captcha_time');
		$word = $this->session->flashdata('captcha_word');

		list($usec, $sec) = explode(" ", microtime());
		$now = ((float)$usec + (float)$sec);

		if ($now - $time > $this->config->item('captcha_expire', 'tank_auth')) {
			$this->form_validation->set_message('_check_captcha', $this->lang->line('auth_captcha_expired'));
			return FALSE;

		} elseif (($this->config->item('captcha_case_sensitive', 'tank_auth') AND
				$code != $word) OR
				strtolower($code) != strtolower($word)) {
			$this->form_validation->set_message('_check_captcha', $this->lang->line('auth_incorrect_captcha'));
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Create reCAPTCHA JS and non-JS HTML to verify user as a human
	 *
	 * @return	string
	 */
	function _create_recaptcha()
	{
		$this->load->helper('recaptcha');

		// Add custom theme so we can get only image
		$options = '<script type="text/javascript">var RecaptchaOptions = {theme : "white"};</script>';

		// Get reCAPTCHA JS and non-JS HTML
		$html = recaptcha_get_html($this->config->item('recaptcha_public_key', 'tank_auth'), null, true);

		return $options.$html;
	}

	/**
	 * Callback function. Check if reCAPTCHA test is passed.
	 *
	 * @return	bool
	 */
	function _check_recaptcha()
	{
		$this->load->helper('recaptcha');

		$resp = recaptcha_check_answer($this->config->item('recaptcha_private_key', 'tank_auth'),
				$_SERVER['REMOTE_ADDR'],
				$_POST['recaptcha_challenge_field'],
				$_POST['recaptcha_response_field']);

		if (!$resp->is_valid) {
			$this->form_validation->set_message('_check_recaptcha', $this->lang->line('auth_incorrect_captcha'));
			return FALSE;
		}
		return TRUE;
	}

	private function _set_user_role($user_id) 
	{

		$data = array('user_id' => $user_id, 'role_id' => 9);
		$this->load->model('user_roles_model');
		$this->user_roles_model->insert($data);
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
