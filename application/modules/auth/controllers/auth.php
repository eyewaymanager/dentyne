<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . "modules/auth/controllers/auth_base.php");

class Auth extends Auth_base
{
	public $ci = '';
	function __construct()
	{
		parent::__construct();

		$this->template->set_template('default');
		$this->load->library('auth/tank_auth');
		$this->load->library('auth/access');
		$this->ci = get_instance();
		//$this->template->set_template('admin_template');
		
	}

	function _extra_registration_validation(){
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('accept-terms', 'Accept terms & conditions', 'required');
	}

	function _extra_registration($data){
		$this->load->model('user_roles_model');

		//Get data
		$input = array();
		$input['first_name'] = $this->input->post('first_name');
		$input['last_name'] = $this->input->post('last_name');
		
		
        

        //Admin security
        //if( ! in_array( $role, array('5','6','7') ) ){ $role = '6'; }

		//Set role as user
		//$this->user_roles_model->insert(array('user_id' => $data['user_id'], 'role_id' => $role));

		//Update
		$this->load->model('admin/user_model');
		$this->user_model->update($data['user_id'], $input);

	}

	function _extra_facebook_registration($data){
		$this->load->model('user_roles_model');

		//Set role as user
		$this->user_roles_model->insert(array('user_id' => $data['user_id'], 'role_id' => '1'));

		//Grab data from facebook?
	}

	public function check_session()
	{
		return $this->tank_auth->is_logged_in();
	}

	public function has_access($role = ""){
        if( ! $this->check_session() ) 
        { 
        	return FALSE; 
        }

		if( $role == "authenticated"  OR $this->access->has_roles($role) )
		{
			return TRUE;
		} 
		else 
		{
			return FALSE;
		}

	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
