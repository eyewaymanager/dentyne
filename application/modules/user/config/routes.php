<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['user'] = "user";
$route['404_override'] = '';
$route['user/pages/(:any)'] = 'page/pages/$1';