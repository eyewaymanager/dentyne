<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends MY_Controller {

	function __construct() {
		parent::__construct();
	}

	public function pages($slug) {


		$this->data['slug'] = $slug;
		$this->template->write_view('menu', 'home/user_menu_bar', $this->data);
		$this->template->write_view('content', 'user/pages', $this->data);
		$this->template->render();
	}
}