<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller{

	function __construct(){
		parent::__construct();
		
		if($this->data['logged_in'] === FALSE && $this->data['is_user'] == FALSE)
			redirect('/auth/login');
	}

	function index(){
		
		$this->template->write_view('menu', 'home/user_menu_bar', $this->data);
		$this->template->write_view('content', 'user/user_main', $this->data);
		$this->template->render();
	}
}