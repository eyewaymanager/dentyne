<!--
	/**
	 * Global Email Template.
	 * Loads partial email
	 */
-->
<body style="font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif; color: #555555; font-size:14px; background: #e7f7ff; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0;padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr>
			<td valign="top" style="text-align:center;">
				<table border="0" cellspacing="0" style="text-align: left;">	
					
				<table  style=" width: 602px; background: white; border: 1px solid gray; font-size: 12px; margin-top: 25px; padding: 25px; position:relative; margin-left:auto; margin-right:auto;">
					<tr>
						<td>
					<p>
						<a href="http://www.winwithdentyne.ca"><img src="<?php echo base_url(lang('email_header_image')) ?>" alt=" Dentyne"/></a>
					</p>
				</td>
			</tr>

					<tr>
<td colspan="100">
        <h2 style="color: #1f4660;"><?php echo lang('email_congratulations') ?>&nbsp; <?php echo $first_name ?> &nbsp; <?php echo $last_name ?></h2>

        <p><?php echo lang('congratulations_text') ?>
        </p>
</td>
</tr>
<tr>	
<td colspan="100">
	<h2> <?php echo lang('pin_code_header') ?> &nbsp; <?php echo $pin_code ?> </h2>
</td>

</tr>
<tr>
<td colspan="100">

             <p><?php echo lang('prize_redeem_explain') ?></p>
			<p><?php echo lang('pin_expiration_explain') ?>&nbsp; <?php echo lang('email_rules_link') ?></p>
 </td>
 </tr>
 <tr>
 	<td colspan="100">
 		<h3><?php echo lang('thank_you') ?></h3>
 	</td>
 </tr>
				</table>
					
					<div id="footer" style="text-align: center; font-weight: bold; font-size:12px">
						BSTREET Communications Inc. On behalf of Dentyne
						<br>
						
					</div>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>