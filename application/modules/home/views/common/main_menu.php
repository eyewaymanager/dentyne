<?php
	$redirect = uri_string();
	$toggle_language = ($this->session->userdata('language') == 'english' || !$this->session->userdata('language')) ? 'french' : 'english';
	

?>
<div class="header clearfix">
	
				<a href="/?language=<?=$language?>&step=<?=$home_step?>&facebook=<?php echo $facebook ?>" class="logo">Dentyne</a>

				<a href="#" class="nav-mobile">Mobile Navigation Button</a>

				<div class="nav">
					<ul>
						<li>
							<a href="/home/prize?language=<?=$language?>&step=<?=$step?>&facebook=<?php echo $facebook ?>" class="menu-link"><?php echo lang('prize_details') ?></a>
						</li>

						<li>
							<a href="/home/rules?language=<?=$language?>&step=<?=$step?>&facebook=<?php echo $facebook ?>" class="menu-link"><?php echo lang('official_rules') ?></a>
						</li>

						<li class="last">
							<a href="home/select_language/<?php echo $toggle_language ?>?redirect=<?php echo $redirect ?>&step=<?=$step?>&facebook=<?php echo $facebook ?>" id="language_selector" class="menu-link"> <?php echo lang($toggle_language) ?></a>
						</li>
					</ul>
				</div><!-- /.nav -->
			</div><!-- /.header -->