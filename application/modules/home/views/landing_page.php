<body class="page-home">
<div class="wrapper">
	<div class="container">
		<div class="shell">
			<div class="header clearfix">
				<a href="#" onclick="return false;" class="logo">Dentyne</a>

				<h2>
					<?php echo lang('like_title') ?>

					<i class="ico ico-arrow"></i>
				</h2>				
			</div><!-- /.header -->

			<div class="main">
				<div class="content">
					<div class="intro">
						<div class="intro-body">
							<h2>
								<?php echo lang('like_text') ?> 
							</h2>

							<h3><?php echo lang('trip_to_hollywood') ?></h3>
						</div><!-- /.intro-body -->
					</div><!-- /.intro -->
				</div><!-- /.content -->
			</div><!-- /.main -->
		</div><!-- /.shell -->
	</div><!-- /.container -->
		