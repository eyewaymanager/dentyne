<div class="main">
				<div class="content">
					<div class="section">
						<h2 class="section-title"><?php echo lang('sorry') ?></h2><!-- /.section-title -->

						<div class="section-body">
							<p>
								<?php echo lang('winning_error_text') ?> 
							</p>
						</div><!-- /.section-body -->

						<div class="section-foot">
							<p><?php echo lang('share_text') ?></p>

							<div class="section-actions">
								<a href="#" class="btn" id="facebook-share-btn" onClick="ga('send', 'event', 'button', 'click', 'Facebook-<?php echo $language!='' ? $language : 'english' ?>', 1);">
									<i class="ico ico-facebook"></i>

									Facebook
								</a>
								
								<a id="twitter-share-btn" href="https://twitter.com/share?url=<?php echo lang('twitter_url') ?>&text=<?php echo urlencode(lang('twitter_share_text')) ?>" class="btn popup" onClick="ga('send', 'event', 'button', 'click', 'Twitter-<?php echo $language!='' ? $language : 'english' ?>', 1);">
									<i class="ico ico-twitter"></i>

									Twitter
								</a>
							</div><!-- /.section-actions -->
						</div><!-- /.section-foot -->
					</div><!-- /.section -->
				</div><!-- /.content -->
			</div><!-- /.main -->
		</div><!-- /.shell -->
	</div><!-- /.container -->