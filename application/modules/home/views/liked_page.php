<form class="form-horizontal landing-page" action="/" method="post" id="landing-page">
  <input type="hidden" name="enter-now" value="1" />
  <input type="hidden" name="facebook_id" value="" />
  <input type="hidden" name="is_facebook" value="<?php echo $facebook ?>" />
  <?php if( $language ): ?>
  <input type="hidden" name="language" value="<?php echo $language ?>" />
  <?php endif; ?>
 <div class="main">
        <div class="content">
          <div class="intro">
            <div class="intro-body">
              <h2>
                <?php echo lang('like_text') ?>
              </h2>

              <h3><?php echo lang('trip_to_hollywood') ?></h3>
            </div><!-- /.intro-body -->

            <div class="intro-actions">
              <input type="submit" class="btn" value="<?php echo lang('enter_now') ?>" id="enter_now" />
            </div><!-- /.intro-actions -->

            <div class="intro-foot">
              <p><?php echo lang('rules_link') ?></p>
            </div><!-- /.intro-foot -->
          </div><!-- /.intro -->
        </div><!-- /.content -->
      </div><!-- /.main -->
    </div><!-- /.shell -->
  </div><!-- /.container -->
</form>