<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
	<title>Dentyne</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="css/images/favicon.ico" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

	<script src="js/facebook/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="js/facebook/functions.js" type="text/javascript"></script>
</head>
<body class="page-coming-soon">
<div class="wrapper">
	<div class="container">
		<div class="shell">
			<div class="header clearfix">
				<a href="#" class="logo">Dentyne</a>
			</div><!-- /.header -->

			<div class="main">
				<div class="content">
					<div class="intro intro-secondary">
						<div class="intro-body">
							<h2><?php echo lang('coming_soon_title') ?></h2>

							<h3><?php echo lang('coming_soon_text') ?></h3>
						</div><!-- /.intro-body -->
					</div><!-- /.intro -->
				</div><!-- /.content -->
			</div><!-- /.main -->
		</div><!-- /.shell -->
	</div><!-- /.container -->
	<div class="footer">
		<div class="shell">
			<div class="footer-nav">
				<ul>
					

					<li>
						<a href="mailto:contest@winwithdentyne.ca">Contact Us</a>
					</li>

					<li>
						<a href="http://www.gagnezavecdentyne.ca/">Francais</a>
					</li>
				</ul>
			</div><!-- /.footer-nav -->
		</div><!-- /.shell -->
	</div><!-- /.footer -->
</div><!-- /.wrapper -->
</body>
</html>