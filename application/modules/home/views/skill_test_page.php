<div class="main">
				<div class="content">
					<div class="section">
						<h2 class="section-title"><?php echo lang('congratulations') ?></h2><!-- /.section-title -->

						<div class="section-body">
							<p>	
								<?php echo lang('skill_test_text') ?>
							</p>

							<div class="form-question">
								<form action="/" method="post" id="skill_test_form">
									<input type="hidden" name="expired" value="" />
									<input type="hidden" name="facebook_id" value="" />
									<input type="hidden" name="is_facebook" value="<?php echo $facebook ?>" />
									<input type="hidden" name="step" value="<?php echo $step ?>" />
									<input type="hidden" name="skill-test-form" value="1" />
									<?php if( $language ): ?>
									<input type="hidden" name="language" value="<?php echo $language ?>" />
									<?php endif; ?>
									<h2 class="form-title"><div id="countdown"></div></h2><!-- /.form-title -->

									<div class="form-row clearfix">
										<label for="field-answer" class="form-label">(5 + 10) x 2 - 10</label>
									
										<div class="form-controls">
											<input type="text" class="field validate[required]" name="field-answer" id="field-answer" />
										</div><!-- /.form-controls -->
									</div><!-- /.form-row -->

									<div class="form-actions">
										<input type="submit" class="btn btn-secondary" value="<?php echo lang('submit') ?>" id="skill_test_button" />
									</div><!-- /.form-actions -->
								</form>
							</div><!-- /.form-question -->
						</div><!-- /.section-body -->
					</div><!-- /.section -->
				</div><!-- /.content -->
			</div><!-- /.main -->
		</div><!-- /.shell -->
	</div><!-- /.container -->