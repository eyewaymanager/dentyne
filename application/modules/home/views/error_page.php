<div class="main">
				<div class="content">
					<div class="section">
						<div class="section-body">
							<?php 
								if(isset($message) && $message == 'daily_restriction') : ?>
								<h4> <?php echo lang('daily_limit_text') ?></h4>
								<?php
									else :
									?>
							<h4><?php echo lang('technical_error_text') ?></h4>

							<p>
								<a href="#" class="btn btn-secondary">BACK</a>
							</p>
						<?php endif ?>
						</div><!-- /.section-body -->
					</div><!-- /.section -->
				</div><!-- /.content -->
			</div><!-- /.main -->
		</div><!-- /.shell -->
	</div><!-- /.container -->