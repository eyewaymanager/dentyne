<div class="main">
				<div class="content">
					<div class="form-register">
						<form role="form" id="personal-info-form" method="post" action="/">
							<input type="hidden" name="facebook_id" value="" />
							<input type="hidden" name="is_facebook" value="<?php echo $facebook ?>" />
							<input type="hidden" name="submit_personal_info" value="1" />
							<input type="hidden" name="language" value="<?php echo $language ?>" />
							<div class="form-head">
								<div class="form-alert form-alert-error"></div><!-- /.form-alert form-alert-error -->
							</div><!-- /.form-head -->

							<div class="form-body">
								<div class="form-section clearfix">
									<div class="form-col">
										<div class="form-row">
											<label for="field-upc" class="form-label form-label-primary"><?php echo lang('enter_your_upc_code') ?></label>

											<div class="form-controls">
												<input type="text" class="field field-primary validate[required, ajax[upc_ajax_check]]" name="field-upc" id="field-upc" value="" title="<?php echo lang('upc') ?>" placeholder="<?php echo lang('upc') ?>" data-prompt-position="topLeft" />
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->
									</div><!-- /.form-col -->
									
									<div class="form-col">
										<div class="form-row form-row-secondary">
											<label for="field-day" class="form-label"><?php echo lang('enter_your_birthday') ?></label>

											<div class="form-controls clearfix">
												<div class="form-col form-col-size1">
													<input type="text" class="field field-size1 validate[required, minSize[2], maxSize[2] autotab" name="field-day" maxlength="2" id="field-day" value="" title="<?php echo lang('birth_dd') ?>" placeholder="<?php echo lang('birth_dd') ?>*" data-prompt-position="topLeft" />	
												</div><!-- /.form-col form-col-size1 -->
												
												<div class="form-col form-col-size1">
													<input type="text" class="field field-size1 validate[required, minSize[2], maxSize[2]] autotab" name="field-month" maxlength="2" id="field-month" value="" title="<?php echo lang('birth_mm') ?>" placeholder="<?php echo lang('birth_mm') ?>*" data-prompt-position="topLeft" />	
												</div><!-- /.form-col form-col-size1 -->
												
												<div class="form-col form-col-size1 birthdate">
													<input type="text" class="field field-size1 validate[required, minSize[4], maxSize[4], ajax[birthday_ajax_check]] autotab" maxlength="4" name="field-year" id="field-year" value="" title="<?php echo lang('birth_yy') ?>" placeholder="<?php echo lang('birth_yy') ?>*" data-prompt-position="topLeft" />
												</div><!-- /.form-col form-col-size1 -->
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->
									</div><!-- /.form-col -->
								</div><!-- /.form-section -->
								
								<div class="form-section clearfix">
									<div class="form-col">
										<div class="form-row">
											<label for="field-first-name" class="form-label form-label-teritary autotab"><?php echo lang('enter_your_information') ?></label>
											
											<div class="form-controls">
												<input type="text" class="field validate[required]" name="field-first-name" id="field-first-name" value="" title="<?php echo lang('first_name') ?>*" placeholder="<?php echo lang('first_name') ?>" data-prompt-position="topLeft" />
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->

										<div class="form-row">
											<div class="form-controls">
												<input type="text" class="field validate[required]" name="field-last-name" id="field-last-name" value="" title="<?php echo lang('last_name') ?>*" placeholder="<?php echo lang('last_name') ?>" data-prompt-position="topLeft" />
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->

										<div class="form-row">
											<div class="form-controls">
												<input type="text" class="field validate[required]" name="field-postal-code" id="field-postal-code" value="" title="<?php echo lang('postal_code') ?>*" placeholder="<?php echo lang('postal_code') ?>" data-prompt-position="topLeft" />
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->

										<div class="form-row">
											<div class="form-controls">
												<input type="text" class="field validate[required, custom[email]]" name="field-email" id="field-email" value="" value="<?php echo lang('email') ?>*" placeholder="<?php echo lang('email') ?>" data-prompt-position="topLeft" />
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->

										<div class="form-row">
											<div class="form-controls">
												<input type="text" class="field" name="field-telephone" id="field-telephone" value="" title="<?php echo lang('telephone') ?> (<?php echo lang('optional') ?>)" placeholder="<?php echo lang('telephone') ?>" data-prompt-position="topLeft" />
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->

										<p class="form-hint"><?php echo lang('required_fields') ?></p><!-- /.form-hint -->
									</div><!-- /.form-col -->
									
									<div class="form-col">
										<div class="form-row">
											<ul class="list-checkboxes">
												<li>
													 <div class="checkbox">
												        <input type="checkbox" id="checkbox_terms" class="validate[required]" name="checkbox_terms" data-prompt-position="topLeft" />

												        <label for="checkbox_terms"><?php echo lang('rules_accept_text') ?></label>
												    </div><!-- /.checkbox -->
												</li>

												<li>
													 <div class="checkbox">
												        <input type="checkbox" id="checkbox_newsletter" class="" name="checkbox_newsletter" value="1" />

												        <label for="checkbox_newsletter"><?php echo lang('newsletter_accept_text') ?></label>
												    </div><!-- /.checkbox -->
												</li>
											</ul><!-- /.list-checkboxes -->
										</div><!-- /.form-row -->

										<div class="form-row">
											<label for="field-captcha" class="form-label"><?php echo lang('enter_words_bellow') ?></label>
											
											<div class="form-controls">
												<div id="captcha_image"></div>

												<input type="text" class="field field-small field-captcha validate[required, ajax[captcha_ajax_check]]" name="recaptcha_response_field" id="recaptcha_response_field" data-prompt-position="bottomLeft" />
												<span class="reload"><a href="#" id="reload_captcha" ><?php echo lang('reload') ?></a></span>
											</div><!-- /.form-controls -->
										</div><!-- /.form-row -->
									</div><!-- /.form-col -->
								</div><!-- /.form-section -->
							</div><!-- /.form-body -->

							<div class="form-actions">
								<input type="submit" class="btn btn-secondary" value="<?php echo lang('submit') ?>" onClick="ga('send', 'event', 'button', 'click', 'submit-<?php echo $language!='' ? $language : 'english' ?>', 1);" />

								<p class="form-hint"><?php echo lang('form_hint') ?></p>
							</div><!-- /.form-actions -->
						</form>
					</div><!-- /.form-register -->
				</div><!-- /.content -->
			</div><!-- /.main -->
		</div><!-- /.shell -->
	</div><!-- /.container -->