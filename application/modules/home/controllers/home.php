<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home controller will handle all public page rendering and logic
 */
class Home extends MY_Controller {

	
	
	//public $step = '';

	function __construct(){
		parent::__construct();
		
		$this->load->library('template');
		$this->load->library('facebook');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->helper('captcha');
		$this->load->model('home/home_model');

		if($this->input->post('signed_request') != '') {

			$this->signed_request = $this->facebook->getSignedRequest();

			if( !is_null($this->signed_request))
				$this->session_signed_request = $this->signed_request;

			if(is_array($this->session_signed_request) && array_key_exists('page', $this->session_signed_request))
				$this->fb_liked = $this->session_signed_request['page']['liked'];

			if( ! $this->selected_language && $this->signed_request && isset($this->signed_request['user']) && isset($this->signed_request['user']['locale'])) {
				$language = strncasecmp('fr', $this->signed_request['user']['locale'], 2) == 0 ? 'french' : 'english';
				$this->selected_language = $language;
			}

			$this->data['facebook'] = $this->input->post('signed_request');

		} else {

			$facebook = $this->input->get_post('facebook');
			if( ! $facebook )
				$facebook = $this->input->get_post('is_facebook');

			if( strlen($facebook) < 7 )
				$facebook = '';

			if( $facebook ) {
				$parsed_request = $this->_parse_signed_request($facebook);
				$this->fb_liked = $parsed_request['page']['liked'];
				
				if( ! $this->selected_language && $parsed_request && is_array($parsed_request)
						&& isset($parsed_request['user']) && is_array($parsed_request['user']) 
						&& isset($parsed_request['user']['locale']) && is_scalar($parsed_request['user']['locale'])
					) {
					$language = strncasecmp('fr', $parsed_request['user']['locale'], 2) == 0 ? 'french' : 'english';
					$this->selected_language = $language;
				}
			}
			$this->data['facebook'] = $facebook;
		}

		//We check if the user is already logged in and if so we redirect to admin

		if( $this->input->get_post('language') ) {
			$language = trim($this->input->get_post('language')) == 'french' ? 'french' : 'english';
			$this->session->set_userdata('language', $language);
			$this->selected_language = $language;
		}

		$this->data['selected_language'] = $this->selected_language;
		$this->data['language'] = $this->selected_language;
		$this->init_language();

		$this->data['step'] = $this->step = $this->input->get_post('step') ? $this->input->get_post('step') : '';// $this->session->userdata('step');


		if( $this->data['facebook'] &&  $this->fb_liked == false)
			$this->data['home_step'] = 1;
		else 
			$this->data['home_step'] = 2;
	}
	
	/**
	 * Landing page rendering and logic
	 *
	 * @return void 
	 */
	public function index()
	{
		if($this->step == '') {
			if($this->data['facebook']!= '' &&  $this->fb_liked == false)
				$this->step = 1;
			else if( $this->data['step'] == 3 || $this->input->post('enter-now') == 1 || $this->input->post('submit_personal_info') || $this->input->post('skill-test-form') )
				$this->step = 3;
			else
				$this->step = 2;
		} 
		
		$this->data['step'] = $this->step;

		switch ($this->step) {
			case '1':
				
				$this->data['body_class'] = 'page-home';
				$this->template->write_view('content', 'home/landing_page', $this->data);
				$this->template->render();
				break;
			
			case '2':

				$this->data['body_class'] = 'page-home';
				$this->template->write_view('menu', 'home/common/main_menu', $this->data);
				$this->template->write_view('content', 'home/liked_page', $this->data);
				$this->template->render();
				break;

			case '3':

				if($this->input->post('submit_personal_info')) {
					$daily_restriction = $this->_check_daily_restriction();
					if(!$daily_restriction) {
						$this->_save_form_data();
						$is_winner = $this->_check_winner();
						if($is_winner) {
							$this->session->set_flashdata('is_winner', $is_winner);
							$this->data['body_class'] = 'page-skill-question';
							$this->template->write_view('menu','home/common/main_menu', $this->data);
							$this->template->write_view('content', 'home/skill_test_page', $this->data);
							$this->template->render();
						} else {
							$this->data['body_class'] = 'page-sorry';
							$this->template->write_view('menu','home/common/main_menu', $this->data);
							$this->template->write_view('content', 'home/didnt_win_page', $this->data);
							$this->template->render();
						}
					} else {
						$this->data['message'] = 'daily_restriction';
						$this->data['body_class'] = 'page-submission-error';
						$this->template->write_view('menu','home/common/main_menu', $this->data);
						$this->template->write_view('content', 'home/error_page', $this->data);
						$this->template->render();
					}
				} else if($this->input->post('skill-test-form')) {

					if($this->input->post('expired') != 1) {
						$skill_test_result = $this->_check_skill_question();

						if($skill_test_result) {
							$this->_send_winner_email($this->session->flashdata('is_winner'));
							$this->data['body_class'] = 'page-success';
							$this->template->write_view('menu','home/common/main_menu', $this->data);
							$this->template->write_view('content', 'home/success_page', $this->data);
							$this->template->render();		
						} else {
							$this->home_model->reliease_pin_code($this->session->flashdata('is_winner'));
							$this->data['body_class'] = 'page-sorry';
							$this->template->write_view('menu','home/common/main_menu', $this->data);
							$this->template->write_view('content', 'home/sorry_page', $this->data);
							$this->template->render();
						}
					} else {
						$this->data['body_class'] = 'page-sorry';
						$this->template->write_view('menu','home/common/main_menu', $this->data);
						$this->template->write_view('content', 'home/sorry_page', $this->data);
						$this->template->render();
					}
				} else {

					$this->load->module('auth');
					$this->data['body_class'] = 'page-form';
					$this->data['captcha'] = $this->auth->_create_recaptcha();
					$this->data['language'] = $this->selected_language;
					//var_dump($this->selected_language);
					$this->template->write_view('menu','home/common/main_menu', $this->data);
					$this->template->write_view('content', 'home/personal_info_form', $this->data);
					$this->template->render();
			}
				break;
		}
		


	}

	public function rules() {
		$this->data['step'] = $this->step;
		//$this->data['facebook'] = $this->input->get('facebook');
		$this->data['body_class'] = 'page-rules';
		$this->template->write_view('menu','home/common/main_menu', $this->data);
		$this->template->write_view('content', 'home/rules_page', $this->data);
		$this->template->render();
	}

	public function prize() {
		$this->data['step'] = $this->step;
		//$this->data['facebook'] = $this->input->get('facebook');
		$this->data['body_class'] = 'page-prize';
		$this->template->write_view('menu','home/common/main_menu', $this->data);
		$this->template->write_view('content', 'home/prize_page', $this->data);
		$this->template->render();
	}

	public function faq() {
		$this->data['step'] = $this->step;
		//$this->data['facebook'] = $this->input->get('facebook');
		$this->data['body_class'] = 'page-rules';
		$this->template->write_view('menu','home/common/main_menu', $this->data);
		$this->template->write_view('content', 'home/faq_page', $this->data);
		$this->template->render();
	}

	public function sorry() {
		$this->data['step'] = $this->step;
		//$this->data['facebook'] = $this->input->post('signed_request') ? $this->input->post('signed_request') : $this->input->get_post('facebook');
		$this->data['body_class'] = 'page-sorry';
		$this->template->write_view('menu','home/common/main_menu', $this->data);
		$this->template->write_view('content', 'home/sorry_page', $this->data);
		$this->template->render();
	}

	public function coming_soon() {
		$this->template->set_template('coming_soon_template');
		
	 	$this->template->write_view('content', 'home/coming_soon_page', $this->data);
		$this->template->render();
	}

	/**
	 * Checks UPC code validity via ajax request on the form and 
	 * sends response accordingly
	 * @return object A json object containing the field id and the response status
	 */
	public function upc_ajax_check() 
	{
		//we parse the request and get all params
		$request_params = $this->input->post();

		//we initialize the response array
		$response = array();

		$field_id = $request_params['fieldId'];
		$field_value = $request_params['fieldValue'];

		//check UPC code validity 
		$result = $this->home_model->upc_ajax_check($field_value);

		//Assigning results to the response array
		$response[0] = $field_id;
		$response[1] = $result;

		//We format the response array as a json object and echo it.
		echo json_encode($response);

	}

	/**
	 * Checks if the user is over 18 using the birthday date 
	 * entered in the form
	 * @return object json object containing the field id and the status of the response
	 */
	public function birthday_ajax_check()
	{
		//we parse all request parameters
		$request_params = $this->input->post();

		//we initialize response array
		$response = array();


		$field_id = $request_params['fieldId'];
		$day = $request_params['field-day'];
		$month = $request_params['field-month'];
		$year = $request_params['fieldValue'];

		//we compare birthday date to the current date 
		$current_date = date_create('now');
		$birthday_date = date_create($year.'-'.$month.'-'.$day);
		$interval = date_diff($current_date, $birthday_date);
		//we format the result as number of years
		$age =  $interval->format('%Y%');


		$response[0] = $field_id;
		$response[1] = true;
		//If age is less than 16 we return false
		if($age < 16)
			$response[1] = false;	

		//we format the response array as a json object and echo it
		echo json_encode($response);


	}
	/**
	 * Check captcha using AJAX before form submission
	 * @return object The response encoded as json object
	 */
	public function captcha_ajax_check()
	{
		

		$request_params = $this->input->post();
		
		$response = array();
		// we need the field id to manipulate validation messages
		$field_id = $request_params['fieldId'];

		//we call a private function that checks captcha 
		$result = $this->home_model->check_captcha();	

		// we build the response array
		$response[0] = $field_id;
		$response[1] = $result;

		echo json_encode($response);
	}

	public function create_captcha()
	{
		$cap = create_captcha(array(
			'img_path'		=> './'.$this->config->item('captcha_path', 'tank_auth'),
			'img_url'		=> base_url().$this->config->item('captcha_path', 'tank_auth'),
			'font_path'		=> './'.$this->config->item('captcha_fonts_path', 'tank_auth'),
			'font_size'		=> $this->config->item('captcha_font_size', 'tank_auth'),
			'img_width'		=> $this->config->item('captcha_width', 'tank_auth'),
			'img_height'	=> $this->config->item('captcha_height', 'tank_auth'),
			'show_grid'		=> $this->config->item('captcha_grid', 'tank_auth'),
			'expiration'	=> $this->config->item('captcha_expire', 'tank_auth'),
		));

		$data = array(
    		'captcha_time'	=> $cap['time'],
    		'ip_address'	=> $this->input->ip_address(),
    		'word'	 => $cap['word']
    	);

		$this->home_model->store_captcha($data);

		$this->ajax_output($cap['image'], true);

	}

	public function select_language($language) {
		
		$redirect = $this->input->get('redirect');
			
		
		if($this->data['facebook'] == '') {
			$this->session->set_userdata('step', $this->step);
			if(trim($language) == 'french')
				redirect('http://www.gagnezavecdentyne.ca/'.$redirect.'?step=' . $this->step, 'location');
			else if(trim($language) == 'english')
				redirect('https://www.winwithdentyne.ca/'.$redirect.'?step=' . $this->step, 'location');
		}
		else {
			$this->session->set_userdata('language', trim($language));
			$this->session->set_userdata('step', $this->step);
			if( $this->input->get('topframe') ) {
				$tab_url = 'https://www.facebook.com/pages/TestApp-Community/742509822456276?id=742509822456276&sk=app_700696786635069';
				redirect( $tab_url );
				return;
			}
			redirect('https://www.winwithdentyne.ca/'.$redirect.'?step=' . $this->step . '&facebook='.$this->data['facebook'].'&language='.$language, 'location');
			//redirect('/?step=' . $this->step . '&facebook='.$this->data['facebook'].'&language='.$language, 'refresh');
		}
	}

	private function _check_daily_restriction() {
		$facebook_id = null;
		$email = null;
			
		if($this->input->post('facebook_id') != '')
			$facebook_id = $this->input->post('facebook_id');
		else
			$email = $this->input->post('field-email');

		$is_restricted = $this->home_model->check_daily_restriction($facebook_id, $email);

		if($is_restricted)
			return TRUE;
		return FALSE;
	}

	/**
	 * performs personal info data manipulation. 
	 * Either creates a new user record if it doesn't exist or
	 * updates the participation table for the particular user
	 * @return void it doesn't return any value
	 */
	private function _save_form_data()
	{

		$form_data = $this->input->post();

		//we clean up and sanitize values submitted by the user.
		foreach ($form_data as $key => $value) {
			if(in_array($key, array('field-upc', 
									'field-day', 
									'field-month', 
									'field-year', 
									'field-first-name', 
									'field-last-name', 
									'field-telephone', 
									'field-email', 
									'field-postal-code', 
									'checkbox_newsletter',
									'facebook_id',
									'language')))
			{
				$this->form_validation->set_rules($key, $key, 'trim|xss_clean');
			} else {
				unset($form_data[$key]);
			}
		}

		//If everything is OK with the data submitted, we call model method 
		//to perform all data manipulation.
		if($this->form_validation->run() === TRUE) {
			//var_dump($form_data);
			$result = $this->home_model->save_form($form_data);
			$this->session->set_userdata('form_data', $form_data);
		}

	}

	/**
	 * We check the skill test answer and return true or false 
	 * accordingly
	 * @return bool true if answer is correct, false if not
	 */
	private function _check_skill_question() {
		$skill_response = $this->input->post('field-answer');

		if($skill_response == 20)
			return TRUE;
		return FALSE;
	}

	private function _check_pin_availability() {
		$is_pin_available = $this->home_model->_check_pin_availability();

		return $is_pin_available;
	}

	/**
	 * Checks if the participant is a winner or not
	 * @return bool TRUE if it's a winner FALSE if not.
	 */
	private function _check_winner() {

		$is_winner = $this->home_model->check_winner();



		if(is_array($is_winner)) {

			return $is_winner;
		}
			
		return FALSE;
	}

	private function _send_winner_email($winning_data) {
		$winner = $winning_data['participant'];
		$pin_code = $winning_data['pin_code'];
		
		$this->data['first_name'] = $winner->first_name;
		$this->data['last_name'] = $winner->last_name;
		$this->data['pin_code'] = $pin_code->pin;

		$this->email->from('winner@winwithdentyne.ca', lang('contest_title'));
		$this->email->to($winner->email);
		$this->email->subject(lang('congratulations_subject'));
		$this->email->message($this->load->view('email/congratulations_email', $this->data, true));
		$this->email->send();

	}

	private function _parse_signed_request($signed_request) {

		list($encoded_sig, $payload) = explode('.', $signed_request, 2);
    	// decode the data
    	$data = json_decode($this->base64UrlDecode($payload), true);
    

    	return $data;
	}

	private function base64UrlDecode($input) {
    	return base64_decode(strtr($input, '-_', '+/'));
  	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */