<?php

class Home_model extends CI_Model{

	function __construct() {
		parent::__construct();

	}
	/**
	 * compares the UPC code entered to the one stored in the database
	 * and returns true or false accordingly
	 * @param  string $upc The UPC code entered in the form
	 * @return bool      True if UPC code is found , false if not
	 */
	public function upc_ajax_check($upc) {

		$this->db->where('code', $upc);
		$result = $this->db->get('upc_code');

		if($result->result())
			return true;
		return false;

	}
	/**
	 * Basic methode that performs data manipulation from the form submitted by the user
	 * @param  array $form_data The array of data submitted by the user
	 * @return void            
	 */
	public function save_form($form_data) {
		
		//we check if the participant already exists in the database
		$participant_exists = $this->_check_participant_existance($form_data);

		//If the participant exists we just update participation table
		//If not we create a new record and update particiaption table
		if($participant_exists) {
			$this->_update_participation($participant_exists->id);
		} else {
			$participant_id = $this->_create_participant($form_data);
			$this->_update_participation($participant_id);
		}
	}

/**
 * Checks if the daily participation limit is reached by the user
 * @param  int $facebook_id User facebook_id
 * @param  str $email       User email
 * @return bool              
 */
	public function check_daily_restriction($facebook_id = null, $email = null) {

		$this->db->select('participations.date_attempted as date_attempted');
		
		$this->db->join('participants', 'participants.id = participations.participant_id', 'left');
		if(!is_null($facebook_id))
			$this->db->where('participants.facebook_id', $facebook_id);
		else
			$this->db->where('participants.email', $email);

		$this->db->order_by('participations.date_attempted', 'desc');
		$this->db->limit(1);
		$result = $this->db->get('participations')->row();
		
		if($result){
			$current_date = date_create('now');
			$date_attempted = date_create($result->date_attempted);
			
			$current_day = $current_date->format('%d%');
			$day_attempted = $date_attempted->format('%d%');
					
			if( $current_day - $day_attempted == 0)
				return TRUE;
		}
		return FALSE;

		
	}

/**
 * Checks if the user is a winner
 * @return mixed Array with participant data and pin code or FALSE if not winner
 */
	public function check_winner() {

		$participant = $this->_check_participant_existance($this->session->userdata('form_data'));
		//var_dump($this->session->userdata('form_data'));
		$is_pin_available = $this->_check_pin_availability();

		if($is_pin_available) {
			$this->_update_winner($participant, $is_pin_available);
			return array('participant' => $participant, 'pin_code' => $is_pin_available);
		}
		return FALSE;

	}


	public function reliease_pin_code($winning_data) {

		$winner = $winning_data['participant'];
		$pin_code = $winning_data['pin_code'];

		$this->db->where('pin_code', $pin_code->pin);
		$this->db->update('pin_codes', array('won' => 0));

		$this->db->order_by('date_attempted', 'desc');
		$participation_id = $this->db->get_where('participations', array('participant_id'=> $winner->id))->row();


		$this->db->where('participations.id', $participation_id->id);
		$this->db->update('participations', array('winner' => 0, 'pin_code' => null));

	}

	public function store_captcha($data) {
		$query = $this->db->insert_string('captcha', $data);
		$this->db->query($query);
	}

	public function check_captcha() {
		$expiration = time()-7200; // Two hour limit
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['fieldValue'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();

		if ($row->count == 0)
		{
    		return false;
		}

		return true;
	}

	/**
	 * Checks participant existance using email or facebook id
	 * @param  array $form_data Array holding user info
	 * @return mixed            Participant info row or FALSE if not found.
	 */
	private function _check_participant_existance($form_data) {

		$this->db->select('*');

		//We use facebook id if present in form data or email if not
		if(isset($form_data['facebook_id']) && $form_data['facebook_id'] !='')
			$this->db->where('facebook_id', $form_data['facebook_id']);
		else
			$this->db->where('email', $form_data['field-email']);

		$result = $this->db->get('participants');

		if($result->num_rows() > 0) {
			return $result->row();
		}

		return FALSE;
	}

	/**
	 * It creates a new record in participants table
	 * @param  array $form_data Array of participant data
	 * @return int            The new record id
	 */
	private function _create_participant($form_data) {

		$insert_data = array();

		$birthday = $form_data['field-day'].'/'.$form_data['field-month'].'/'.$form_data['field-year'];

		$insert_data['first_name'] = $form_data['field-first-name'];
		$insert_data['last_name'] = $form_data['field-last-name'];
		$insert_data['language'] = $form_data['language'];
		if(isset($form_data['facebook_id']))
			$insert_data['facebook_id'] = $form_data['facebook_id'];
		$insert_data['email'] = $form_data['field-email'];
		$insert_data['postal_code'] = $form_data['field-postal-code'];
		$insert_data['telephone'] = $form_data['field-telephone'];
		if(isset($form_data['checkbox_newsletter']))
			$insert_data['newsletter'] = $form_data['checkbox_newsletter'];
		$insert_data['birthday'] = $birthday;

		$query = $this->db->insert('participants', $insert_data);
		$participant_id = $this->db->insert_id();

		return $participant_id;
	}

	/**
	 * Updates participation table each time a participant submits the form
	 * @param  int $participant participant id
	 * @return void              
	 */
	private function _update_participation($participant) {
		$upc_code_id = $this->db->get_where('upc_code', array('code' => $this->input->post('field-upc')))->row()->id;

		$this->db->insert('participations', array('participant_id' => $participant, 'upc_code_id' => $upc_code_id, 'winner' => 0));
	}

/**
 * Checks if there is a pin available for winning
 * @return mixed Pin object or FALSE
 */
	public function _check_pin_availability() {
		$current_time_object = new DateTime();
		$current_time = $current_time_object->format('Y-m-d H:i:s');

		$this->db->select('available_from as available_from, id as id, pin_code as pin, won as won');
		$this->db->where('available_from <= ', $current_time);
		$this->db->where('won !=', 1);
		$this->db->order_by('available_from', 'desc');
		$this->db->limit('1');
		$result = $this->db->get('pin_codes')->row();

		if(count($result) > 0 && $result->won == 0)
			return $result;
		return FALSE;

	}

/**
 * Updates participations table if a winner is found and pin codes table 
 * to remove the won PIN code from the list of available pins
 * @param  object $participant The participant object
 * @param  object $pin_code    tHE PIN code object
 * @return void              
 */
	private function _update_winner($participant, $pin_code) {

		$current_time_object = new DateTime();
		$current_time = $current_time_object->format('Y-m-d H:i:s');

		$participant_id = $participant->id;

		$this->db->order_by('date_attempted', 'desc');
		$participation_id = $this->db->get_where('participations', array('participant_id'=> $participant_id))->row();

		$this->db->where('participations.id', $participation_id->id);
		$this->db->update('participations', array('winner' => 1, 'pin_code' => $pin_code->id));

		$this->db->where('pin_codes.id', $pin_code->id);
		$this->db->where('pin_codes.available_from <=', $current_time);
		$this->db->update('pin_codes', array('won' => 1));

		$this->db->where('pin_codes.available_from <=', $current_time);
		$this->db->update('pin_codes', array('won' => 1));

	}
}