<div class="admin-dashboard">
	<?php $this->view('/partial_article_menu'); ?>
	<div class="admin-content">
		<?php if($articles && count($articles) > 0) { ?>
				
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Published</th>
								<th>URL</th>
								<th>actions</th>
							</tr>
						</thead>
						<?php foreach ($articles as $article) { ?>
						<tr>
							<td>
								<?php echo $article->id ?>
							</td>
							<td>
								<?php echo $article->title ?>
							</td>
							<td>
								<?php echo $article->published ?>
							</td>
							<td>
								<?php echo $article->url ?>
							</td>
							<td>
								<a href="/admin/articles/edit/<?php echo $article->id ?>"><i class="icon-edit"></i> edit</a>
							</td>
						</tr>
						<?php } ?>
					</table>
				<?php } ?>
	</div>
</div>