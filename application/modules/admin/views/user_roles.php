<div class="admin-dashboard">
	<?php $this->view('/partial_user_menu'); ?>
	<div class="admin-content">
		<?php if($user_roles && count($user_roles) > 0) { ?>
				
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Role Name</th>
							</tr>
						</thead>
						<?php foreach ($user_roles as $user_role) { ?>
						<tr>
							<td>
								<?php echo $user_role->id ?>
							</td>
							<td>
								<?php echo $user_role->name ?>
							</td>
							
							<td>
								<a href="/admin/user/edit/<?php echo $user_role->id ?>"><i class="icon-edit"></i> edit</a>
							</td>
						</tr>
						<?php } ?>
					</table>
				<?php } else {
					echo 'No user roles added yet';
				} ?>

	</div>	

</div>