<div class="admin-dashboard">
	<div class="admin-content">
		<?php if(isset($form_errors)) { ?>
			<div class="alert alert-error">
				<div class="error"><?php echo $form_errors ?></div>
			</div>
		<?php } ?>
		<form class="form-horizontal" method="post" action="/admin/user/edit">
			<input type="hidden" name="id" value="<?php echo isset($user->id) ? $user->id : '' ?>" />
			<div class="control-group">
				<label class="control-label">email</label>
				<div class="controls">
					<input type="text" name="email" value="<?php echo isset($user->email) ? $user->email : '' ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Username</label>
				<div class="controls">
					<input type="text" name="username" value="<?php echo isset($user->username) ? $user->username : '' ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password</label>
				<div class="controls">
					<input type="text" name="password" value="<?php echo isset($user->password) ? $user->password : '' ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">First Name</label>
				<div class="controls">
					<input type="text" name="first_name" value="<?php echo isset($user->first_name) ? $user->first_name : '' ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Last Name</label>
				<div class="controls">
					<input type="text" name="last_name" value="<?php echo isset($user->last_name) ? $user->last_name : '' ?>" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Is Active</label>
				<div class="controls">
					<input type="checkbox" name="activated" value="1" <?php echo $user->activated == 1 ? 'checked="checked"' : '' ?> />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">User Role</label>
				<div class="controls">
					<select name="user_role"  />
						<option value="9" <?php echo isset($user->role) && $user->role == 'user' ? 'selected="selected"' : '' ?> >user</option>
						<option value="10" <?php echo isset($user->role) && $user->role == 'admin' ? 'selected="selected"' : '' ?> >admin</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				
				<div class="controls">
					<button type="submit" name="save" class="btn" />Save</button>
				</div>
			</div>
		</form>
	</div>
</div>