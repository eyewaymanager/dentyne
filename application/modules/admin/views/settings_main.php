<div class="admin-dashboard">
	<div class="admin-content">
		<?php if(isset($form_errors)) { ?>
			<div class="alert alert-error">
				<div class="error"><?php echo $form_errors ?></div>
			</div>
		<?php } ?>
		<form class="form-horizontal" method="post" action="/admin/settings" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo isset($settings->id) ? $settings->id : '' ?>" />
			<div class="control-group">
				<label class="control-label">Title</label>
				<div class="controls">
					<input type="text" name="title" value="<?php echo isset($settings->title) ? $settings->title : '' ?>" class="input-xxlarge" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Desription</label>
				<div class="controls">
					<textarea  name="description" value="" class="input-xxlarge" >
						<?php echo isset($settings->description) ? $settings->description : '' ?>
					</textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Keywords</label>
				<div class="controls">
					<input type="text" name="keywords" value="<?php echo isset($settings->keywords) ? $settings->keywords : '' ?>" class="input-xxlarge" data-role="tagsinput" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Logo</label>
				<div class="controls">
					<img src="<?php echo isset($settings->logo) ? $settings->logo : '' ?>" class="img-rounded" />
					<input type="file" name="logo" value="" class="input-xxlarge" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Copyright</label>
				<div class="controls">
					<input type="text" name="copyright" value="<?php echo isset($settings->copyright) ? $settings->copyright : '' ?>" class="input-xxlarge"  />
				</div>
			</div>
			<div class="control-group">
				
				<div class="controls">
					<button type="submit" name="save" class="btn" />Save</button>
				</div>
			</div>
		</form>
	</div>
</div>