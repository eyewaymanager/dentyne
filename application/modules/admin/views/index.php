<div class="admin-dashboard">
	
	<a href="/admin/export_csv">
		<button class="btn btn-primary btn-lg pull-left">
			Export CSV 
		</button>
	</a>
	<div class="clearfix"></div>
	<div class="users_grid"></div>	
	<script type="text/x-kendo-template" id="template"><div class="details"></div></script>

</div>