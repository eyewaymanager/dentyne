<div class="admin-dashboard">
	<div class="admin-content">
		<?php if(isset($form_errors)) { ?>
			<div class="alert alert-error">
				<div class="error"><?php echo $form_errors ?></div>
			</div>
		<?php } ?>
		<form class="form-horizontal" method="post" action="/admin/articles/edit" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo isset($article->id) ? $article->id : '' ?>" />
			<div class="control-group">
				<label class="control-label">Title</label>
				<div class="controls">
					<input type="text" name="title" value="<?php echo isset($article->title) ? $article->title : '' ?>" class="input-xxlarge" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Desription</label>
				<div class="controls">
					<textarea  name="description" value="" class="input-xxlarge" >
						<?php echo isset($article->description) ? $article->description : '' ?>
					</textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Content</label>
				<div class="controls">
					<textarea name="content" value="" class="input-xxlarge" >
						<?php echo isset($article->content) ? $article->content : '' ?>
					</textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Publish</label>
				<div class="controls">
					<?php $checked = isset($article->published) && $article->published == 1 ? 'checked="checked"' : '' ?>
					<input type="checkbox" name="published" value="1" class="input-xxlarge" <?php echo $checked ?> />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Tags</label>
				<div class="controls">
					<input type="text" name="tags" value="<?php echo isset($article->tags) ? $article->tags : '' ?>" class="input-xxlarge" data-role="tagsinput" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Thumb</label>
				<div class="controls">
					<img src="<?php echo isset($article->thumb_url) ? $article->thumb_url : '' ?>" class="img-rounded" />
					<input type="file" name="thumb_url" value="<?php echo isset($article->thumb_url) ? $article->thumb_url : '' ?>" class="input-xxlarge" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Video</label>
				<div class="controls">
					<input type="text" name="video_url" value="<?php echo isset($article->video_url) ? $article->video_url : '' ?>" class="input-xxlarge"  />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Category</label>
				<div class="controls">
					<select name="category_id"  />
						<option value="9" <?php echo isset($user->role) && $user->role == 'user' ? 'selected="selected"' : '' ?> >user</option>
						<option value="10" <?php echo isset($user->role) && $user->role == 'admin' ? 'selected="selected"' : '' ?> >admin</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				
				<div class="controls">
					<button type="submit" name="save" class="btn" />Save</button>
				</div>
			</div>
		</form>
	</div>
</div>