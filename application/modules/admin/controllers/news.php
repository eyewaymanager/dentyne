<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MY_Controller{


	public function __construct() {
		parent::__construct();

		if($this->data['logged_in'] === FALSE)
			redirect('/auth/login');
	}

	public function index() {

		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/news_main', $this->data);
		$this->template->render();
	}
}