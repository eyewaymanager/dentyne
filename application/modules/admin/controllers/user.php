<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Admin_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('admin/user_model');
		if($this->data['logged_in'] === FALSE && $this->data['is_admin'] ==FALSE)
		 	redirect('/auth/login');
	}

	public function index() {

	}

	public function edit($id = '') {

		if($id == '')
			$id = $this->input->post('id');

		$user = $this->user_model->get_user($id);
		$post_data = $this->input->post();
		
		if($post_data) {
			$is_valid = $this->_validate_user_form();
			if($is_valid) {
				$this->_save_user_form($user);
				redirect('/admin');
			}
			else {
				$this->data['form_errors'] = validation_errors();
				$user = (object)$post_data;
			}
		}
		$this->data['user'] = $user;

		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/user_edit', $this->data);
		$this->template->render();
	}

	private function _validate_user_form() {
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');


		if($this->form_validation->run() === TRUE)
			return TRUE;
		return FALSE;
	}

	private function _save_user_form($user) {
		$user = $this->user_model->create_new();
		$post_data = $this->input->post();

		foreach ($post_data as $key => $value) {
			$user->$key = $value;
		}
		$activated = array_key_exists('activated', $post_data) ? 1 : 0;
		$user->activated = $activated;
		$result = $user->save();

		$this->_update_user_role($user);


		return $result;
	}

	private function _update_user_role($user) {
		
		if(isset($user->user_role)) {
			$this->user_model->update_user_role($user);
		}
	}
	public function user_roles() {

		$user_roles = $this->user_model->get_user_roles();

		$this->data['user_roles'] = $user_roles;

		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/user_roles', $this->data);
		$this->template->render();
	}
}