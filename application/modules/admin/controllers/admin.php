<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public $logged_in = FALSE;
	function __construct(){
		
		parent::__construct();
		
		$this->load->library('template');
		
		$this->load->model('user_model');
		
		
	}
	
	public function index()
	{	
		
		if($this->data['logged_in'] === FALSE)
			redirect('/auth/login');

		$this->data['is_admin'] = TRUE;
		$users = $this->user_model->get_all();
		if($this->input->is_ajax_request()){
			$user_array = $this->_create_client_array($users['data']);
		 	$this->ajax_output($user_array, $users['total'], true);
		}

		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/index', $this->data);
		$this->template->render();


	}

	public function participations() {
		$participations_array = $this->user_model->get_participations();

		$this->ajax_output($participations_array['data'], $participations_array['total'], true);
		//echo json_encode($participations_array);
	}

	public function init_pin_codes() {
		$start_date = new DateTime('03-06-2014 12:00:00');

	 $this->setting_model->init_pin_codes($start_date);


	}

	public function export_csv() {

		$array = $this->user_model->create_csv_array();

		$this->download_send_headers("data_export_" . date("Y-m-d") . ".csv");
		echo $this->array2csv($array);
		die();
	}

	private function _create_client_array($result) 
	{
		$client_arr = array();
		//$client_table 
		//= $this->load->view('clients/clients_table', $this->data, true);
		foreach ($result as $index => $row) {
			$client_arr[$index]['id'] = $row->id;
			$client_arr[$index]['first_name'] = $row->first_name;
			$client_arr[$index]['last_name'] = $row->last_name;
			$client_arr[$index]['email'] = $row->email;
			$client_arr[$index]['telephone'] = $row->telephone;
			$client_arr[$index]['postal_code'] = $row->postal_code;
			$client_arr[$index]['birthday'] = $row->birthday;
			$client_arr[$index]['newsletter'] = $row->newsletter;
		}
		return $client_arr;
	}

	private function download_send_headers($filename) {
		// disable caching
	    $now = gmdate("D, d M Y H:i:s");
	    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
	    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
	    header("Last-Modified: {$now} GMT");

	    // force download  
	    header("Content-Type: application/force-download");
	    header("Content-Type: application/octet-stream");
	    header("Content-Type: application/download");

	    // disposition / encoding on response body
	    header("Content-Disposition: attachment;filename={$filename}");
	    header("Content-Transfer-Encoding: binary");
	}

	private function array2csv($array) {
		if (count($array) == 0) {
	     	return null;
	   	}
	   	ob_start();
	   	$df = fopen("php://output", 'w');
	  	 fputcsv($df, array_keys(reset($array)));
	   	foreach ($array as $row) {
	      	fputcsv($df, $row);
	   	}
	   	fclose($df);
	   	return ob_get_clean();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */