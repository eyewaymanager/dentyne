<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends MY_Controller{
	//public $logged_in = FALSE;

	public function __construct() {
		parent::__construct();
		
		$this->load->model('admin/article_model');
		if($this->data['logged_in'] === FALSE)
		 	redirect('/auth/login');
		
	}

	public function index() {

		$articles = $this->article_model->get_all();

		$this->data['articles'] = $articles;
		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/articles_main', $this->data);
		$this->template->render();
	}

	public function edit($id = '') {

		$post = $this->input->post();
		if($id != ''){
			$article = $this->article_model->get($id);
			$this->data['article'] = $article;
		} else { 
			$article = $this->article_model->create_new();
		}
			if($post) {
				$valid = $this->_validate_article_form();
				if($valid) {
					$this->_save_article_form($article);
						$result = $this->_save_uploaded_files($article);
					if($result['error'])
						$this->data['form_error'] = $result['error'];
					else
						redirect('/admin/articles');
				}
			}
		

		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/articles_edit', $this->data);
		$this->template->render();
	}

	public function delete($id) {

		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/articles_main', $this->data);
		$this->template->render();
	}

	private function _validate_article_form() {
		$this->form_validation->set_rules('title', 'Title', 'trim|xss_clean|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|xss_clean|required');
		$this->form_validation->set_rules('content', 'Content', 'trim|xss_clean|required');
		$this->form_validation->set_rules('tags', 'Tags', 'trim|xss_clean|required');
		if($this->form_validation->run() ===TRUE)
			return TRUE;
		return FALSE;

	}

	private function _save_article_form($article) {

		$post = $this->input->post();

		foreach ($post as $key => $value) {
			$article->$key = $value;
		}

		$result = $article->save();

		return $result;
	}

	private function _save_uploaded_files($article) {

		if($_FILES['thumb_url']['name'] != ''){ 

			if ( ! $this->upload->do_upload('thumb_url'))
			{
				$error = array('error' => $this->upload->display_errors());

				return $error;
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$article->thumb_url = '/uploads/'.$data['upload_data']['file_name'];	
			}
		} else {
			$article->thumb_url = '/uploads/'.$post['thumb_url'];
		}
		
	    $article->save();
	    return TRUE;
	}
}