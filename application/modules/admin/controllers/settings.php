<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller 
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin/setting_model');
	}

	public function index()
	{
		$settings_array = $this->setting_model->get(1);

		$settings = !empty($settings_array) ? $settings_array : $this->setting_model->create_new();
			
		$post = $this->input->post();

			
			
			if($post) {
				$valid = $this->_validate_settings_form();
				if($valid) {
					$result = $this->_save_settings_form($settings);
					if($result['error'])
						$this->data['form_error'] = $result['error'];
					else
						redirect('/admin/settings');
				}
			}
		
		$this->data['settings'] = $settings;
		$this->template->write_view('menu', 'admin/admin_menu_bar', $this->data);
		$this->template->write_view('content', 'admin/settings_main', $this->data);
		$this->template->render();
	}

	private function _validate_settings_form() {
		$this->form_validation->set_rules('title', 'Title', 'trim|xss_clean|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|xss_clean|required');
		$this->form_validation->set_rules('keywords', 'Keywords', 'trim|xss_clean|required');
		$this->form_validation->set_rules('copyright', 'Copyright', 'trim|xss_clean|required');
		if($this->form_validation->run() ===TRUE)
			return TRUE;
		return FALSE;

	}

	private function _save_settings_form($settings) {

		$post = $this->input->post();
		
		foreach ($post as $key => $value) {
			$settings->$key = $value;
		}
		$settings->logo = $this->_save_uploaded_files($settings);
		$result = $settings->save();

		return $result;
	}

	private function _save_uploaded_files($settings) {

		if($_FILES['logo']['name'] != ''){ 

			if ( ! $this->upload->do_upload('logo'))
			{
				$error = array('error' => $this->upload->display_errors());

				return $error;
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$settings->logo = '/uploads/'.$data['upload_data']['file_name'];	
			}
		} else {
			$settings->logo = '/uploads/'.$post['logo'];
		}
		
	   
	    return $settings->logo;
	}
}