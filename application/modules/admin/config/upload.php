<?php 

$config['upload_path'] = './uploads/';
$config['allowed_types'] = 'gif|jpg|png';
$config['max_size'] = '100000';
$config['max_width'] = '2500';
$config['max_height'] = '1500';