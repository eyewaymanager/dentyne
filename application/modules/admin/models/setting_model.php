<?php

class Setting_model extends MY_Model
{

	

	public function delete_all()
	{
		$this->db->truncate('settings');
	}

	public function init_pin_codes($date_object) {
		$this->db->select('pin_code as code, id as id, available_from as available_from');
		$pin_codes = $this->db->get('pin_codes')->result();

		$new_date = $date_object;


		foreach ($pin_codes as $row) {
			$this->db->where('id', $row->id);		
			$this->db->update('pin_codes', array('available_from' => $new_date->format('Y-m-d H:i:s')));

			$new_date = $date_object->modify('+122 minutes');
			
		}

	}
}