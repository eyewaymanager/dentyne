<?php 

class User_model extends MY_Model{
	
	public function get_all(){
		$this->db->select('participants.email as email, 
						   participants.first_name as first_name, 
						   participants.last_name as last_name,
						   participants.birthday as birthday,
						   participants.postal_code as postal_code,
						   participants.id as id,
						   participants.telephone as telephone,
						   participants.newsletter as newsletter');
		

		$participants = $this->db->get('participants')->result();
		$total = count($participants);
		$result['data'] = $participants;
		$result['total'] = $total;

		if(count($result) > 0)
			return $result;
		return FALSE;
	}
	public function get_user($id) {
		$this->db->select('users.id as id, 
						   users.email as email,
						   users.username as username,
						   users.first_name as first_name,
						   users.password as password,
						   users.last_name as last_name, 
						   users.activated as activated, 
						   ro.name as role');
		$this->db->join('user_roles ur', 'ur.user_id = users.id', 'left');
		$this->db->join('roles ro', 'ro.id = ur.role_id', 'left');
		$this->db->where('users.id', $id);

		$result = $this->db->get('users')->row();

		if(count($result) > 0)
			return $result;
		return FALSE;
	}
	public function get_user_roles() {

		$this->db->select('roles.*');
		$this->db->group_by('name');
		$result = $this->db->get('roles')->result();

		if(count($result) > 0)
			return $result;
		return FALSE;
	}

	public function update_user_role($user) {

		$this->db->where('user_id', $user->id);
		$this->db->update('user_roles', array('role_id' => $user->user_role));
	}

	public function get_participations() {
		$participations = array();
		$filter = $this->input->get('filter');
		$limit = $this->input->get('take');
		$offset = $this->input->get('skip');
		$participant_id = $filter['filters'][0]['value'];
		$order = $this->input->get('sort') ? $this->input->get('sort') : false;


		$this->db->select('participations.date_attempted as date_attempted,
						   participations.winner as is_winner,
						   pin_codes.pin_code as pin_code');
		$this->db->join('participants', 'participations.participant_id = participants.id', 'left');
		$this->db->join('pin_codes', 'participations.pin_code = pin_codes.id', 'left');
		$this->db->where('participations.participant_id', $participant_id);
		
		if($order)
			$this->db->order_by($order[0]['field'], $order[0]['dir']);
		$this->db->limit($limit, $offset);
		$participations = $this->db->get('participations')->result();
		$total = count($participations);
		$result['data'] = $participations;
		$result['total'] = $total;
		if(count($result) > 0)
			return $result;
		return false;
	}

	public function create_csv_array() {

		$this->db->select('participants.first_name as FirstName,
						   participants.last_name as LastName,
						   participants.email as Email,
						   participants.postal_code as PostalCode,
						   participants.telephone as Telephone,
						   participants.birthday as BirthDate,
						   participants.language as Language,
						   participants.date_created as DateSubmitted,
						   participants.newsletter as NewsletterAgree,
						   pin_codes.pin_code as PIN,
						   participations.winner as IsWinner
						   ');
		$this->db->distinct();
		$this->db->join('participations', 'participations.participant_id = participants.id', 'left');
		$this->db->join('pin_codes', 'pin_codes.id = participations.pin_code', 'left');
		//$this->db->where('participations.winner', 1);
		$result = $this->db->get('participants')->result_array();

		return $result;
	}
}