$(document).ready(function(){
  if($(window).width() < 500){

  }
  else{
    var $StickyFooter = $(document.getElementById("sticky-footer")),
        StickyHeight = $StickyFooter.outerHeight(),
        WindowHeight = $(window).height();

    var StickyPosition = (Number($("#sticky-footer").offset().top));

    var config = { threshold: -190, offset_intertia: 0.35, top_offset: (StickyHeight) };

    $(".slide-1").css("height",$(window).height());



    var topSticky = function(){
      if($(document).scrollTop() < ($("#lowerArea").offset().top - WindowHeight)){
        $StickyFooter.css({position: 'fixed', bottom: '0', top: 'auto'});
      }
      else{
        $StickyFooter.css({position: 'absolute', marginTop: -StickyHeight, bottom:'auto'});
        $(window).on('scroll',bottomSticky);
        $(window).off('scroll',topSticky);
      }
    };

    var bottomSticky = function(){
      if( $(document).scrollTop() >= ($("#lowerArea").offset().top - StickyHeight) ){
        $StickyFooter.css({position: 'fixed', top: config.top_offset + 'px'});
      }
      else{
        $StickyFooter.css({
          position: 'absolute',
          marginTop: -StickyHeight,
          top:'auto',
          bottom:'auto'
        });
        $(window).on('scroll',topSticky);
        $(window).off('scroll',bottomSticky);
      }
    };

    var load_sticky = function(){
      $(window).on('scroll',topSticky)
    }

    setTimeout(load_sticky, 1000);


    $(".story").each(function() {
      var $self = $(this);
      $("h1", $self).css("marginTop", (WindowHeight/2) - $("h1", $self).outerHeight());
      if($self.hasClass("slide-1")){
        $("h1", $self).css("marginTop", (WindowHeight/2) - ($("h1", $self).outerHeight()/2));
        $("#wishlistVideo").css("marginTop", (WindowHeight/2) - $("h1", $self).outerHeight() - 100);
      }
      
     
    });

    function _scroll_background(el) {
      var bgp = el.backgroundPosition().split(' ');
      var bpos = bgp[0] + (WindowHeight/2.5-($.distancefromfold(
      el,{threshold:config.threshold}))/2) + 'px';
      el.css({'background-position':bpos})

      h1distance = $.distancefromfold($("h1", el),{threshold:config.threshold});
      if(h1distance < (WindowHeight/2)){
        // h1 is belown midway
        h1percent = (h1distance / (WindowHeight/2)).toFixed(2);
        $("h1", el).css("opacity", h1percent);
      }
      else if(h1distance > (WindowHeight/2)){
        // h1 is above midway
        h1percent = ((WindowHeight - h1distance) / (WindowHeight/2)).toFixed(2);
        $("h1", el).css("opacity", h1percent);
      }
    }

    function handleScroll(el){
      var slideShown = false;
      $(el).bind('inview', function(event, visible, visiblePartX, visiblePartY){
        if(visible){
          if(slideShown == false){
            $('html,body').animate({scrollTop: $(this).offset().top},'slow');
            slideShown = true;
          }
        }
        else{
          slideShown = false;
        }
      });
    }

    jQuery.fn.backgroundPosition = function() {
      var p = $(this).css('background-position');
      if(typeof(p) === 'undefined') return $(this).css('background-position-x') +
      ' ' + $(this).css('background-position-y');
      else return p;
    };

    function ScrollPage(el){
      var $triggerParent = el.closest(".story");
      if($triggerParent.hasClass("last") == false){
        $.smoothScroll({
          scrollTarget: $triggerParent.next(".story"),
          easing: 'easeInOutQuad',
          speed: 1000
        });
        return false;
      }
      else if($triggerParent.hasClass("last")){
        $.smoothScroll({
          offset:-($("#sticky-footer").outerHeight()),
          scrollTarget: $("#lowerArea"),
          easing: 'easeInOutQuad',
          speed: 1000
        });
        return false;
      }
    }

    $(".trigger-area").click(function(e){
      ScrollPage($(this))
    });

    $(".container h1").click(function(e){
      ScrollPage($(this))
    });

  }

  
 });

      
    
  