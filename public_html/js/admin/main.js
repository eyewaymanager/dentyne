$(document).ready(function(){

  $('#main_menu').kendoMenu();
  $('#users-menu').kendoMenu();

  $('.users_grid').kendoGrid({
  	dataSource: {
  		transport: {
  			read: {
  				url: '/admin',
  				type: 'get'
  			}
  		},
  		schema: {
				data: 'data',
				total: 'total',
				model: {
					id: 'id',
        	fields: {
              first_name: { editable: true },
              last_name: { editable: true},
              email : {editable: true},
              telephone : {editable:true},
              birthday : {editable: true},
              postal_code : {editable: true},
              newsletter : {editable:true}
        	}
        }
			},
  		pageSize: 10
  	},
  	groupable: true,
  	sortable: true,
  	pageable: {
  		refresh: true,
  		pageSizes: false,
  		
  	},
    detailTemplate: kendo.template($("#template").html()),
                        detailInit: detailInit,
                        dataBound: function() {
                            this.expandRow(this.tbody.find("tr.k-master-row").first());
                        },
  	columns: [{
  		field: 'first_name',
  		title: 'First Name',
  		width: 200
  	}, {
  		field: 'last_name',
  		title: 'Last Name',
  		width: 200
  	}, {
  		field: 'email',
  		title:' Email',
  		width: 200
  	}, {
      field: 'telephone',
      title:'Telephone',
      width: 120
    },
      {
  		field: 'birthday',
  		title: 'Birthday',
  		width: 80
  	}, {
  		field: 'postal_code',
  		title: 'Postal Code',
  		width: 80,
  	}, {
      field: 'newsletter',
      title:'Newsletter',
      width: 50
    },
      {
      command: ['edit', 'destroy'], title: '&nbsp;', width: 150
    }],
    selectable: true,
    editable: 'inline'
  });
});


function detailInit(e) {
  var detailRow = e.detailRow;

  detailRow.find(".details").kendoGrid({
      dataSource: {
        transport: {
            read: "/admin/participations"
        },
        schema: {
          data: 'data',
          total: 'total',
          model: {
            id: 'id',
            fields: {
              date_attempted: { editable: true },
              is_winner: { editable: true},
              pin_code : {editable: true},
                
            }
          }
        },
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        pageSize: 10,
        filter: { field: "participant_id", operator: "eq", value: e.data.id }
      },
      scrollable: false,
      sortable: true,
      filterable: {
        extra: false,
        operators: {
          string: {
          	startswith: "Starts with",
            eq: "Is equal to",
            neq: "Is not equal to"
          }
        }
      },
      pageable: {
        refresh: true
      },
      columns: [
          { field: "date_attempted", title:"Date Attempted", filterable: true, width: 100 },
          { field: "is_winner", title:"Is Winner", filterable: true, width: 50 },
          { field: "pin_code", title:"PIN", filterable: false, width: 120 },
          
      ]
  });
}
