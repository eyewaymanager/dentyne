;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function() {

		$doc.on('focusin focusout', '.field, textarea', function(event) {
			if (event.type === 'focusin' && this.value === this.defaultValue) {
				this.value = '';
			} else if (event.type === 'focusout' && this.value === '') {
				this.value = this.defaultValue;
			}
		});

		custom_checkbox('.checkbox input');

		$('.nav-mobile').on('click', function() {
			$(this).toggleClass('active');

			$('.nav').slideToggle('fast');
			$('.wrapper').toggleClass('nav-expanded');

			return false;
		});
		
		resizeContainer();
		showNav();
	});

	$win.load(function() {
		resizeContainer();
		showNav();
	});

	$win.resize(function() {
		resizeContainer();
		showNav();
	});

	function showNav() {
		if ($win.width() < 767) {
			$('.nav').removeClass('visible');
		} else {
			$('.nav').addClass('visible');
		}
	}

	function resizeContainer() {
		var container = $('.container'),
			footer_height = $('.footer').outerHeight(),
			intro = $('.intro');
		if($('#landing-page').length > 0 && ($win.width() < 812 && $win.width() > 767))
			container.css('min-height', 578);
		else if($win.width() <767)
			intro.css('padding-top', $win.width());
		else {
		 	container.css( 'min-height', $win.height() - footer_height );
		 	intro.css('padding-top', 20);
		 }
	}

	function custom_checkbox(items) {
		$(items).each(function() {
			var $this = $(this);
			if(!$this.parents('.custom-checkbox').length) {
				$this.wrap('<div class="custom-checkbox" />').parent().append('<span />');

				if($this.is(':checked')) {
					$this.parent().addClass('checked');
				}
			}

			$(document).on('change', 'input[type="checkbox"]', function() {
				var $this = $(this);
				if($this.is(':checked')) {
					$this.parent().addClass('checked');
				} else {
					$this.parent().removeClass('checked');
				}
			});
		});
	}

	function custom_radio(items) {
		$(items).each(function() {
			var $this = $(this);
			if(!$this.parents('.custom-radio').length) {
				$this.wrap('<div class="custom-radio" />').parent().append('<span />');

				if($this.is(':checked')) {
					$this.parent().addClass('checked');
				}
			}

			$(document).on('change', 'input[type="radio"]', function() {
				var $this = $(this);
				var rad_name = $this.attr('name');
				if($this.is(':checked')) {
					$('input[name="' + rad_name + '"]').each(function() {
						$(this).parent().removeClass('checked');
					});
					$this.parent().addClass('checked');
				}
			});
		});
	}
})(jQuery, window, document);
