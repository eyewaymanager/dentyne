$(document).ready(function(){
	
  //Initialization code for masked Postal Code field
  $('#field-postal-code').focus(function(){

    $(this).mask('S0S 0S0');
  });

  //Initialization code for Telephone field to format Canadia phone numbers
  $('#field-telephone').focus(function(){
    $(this).mask('(000) 000-0000');
  });

  //Initialization code for UPC field limit to 10 digits no spaces allowed
  $('#field-upc').focus(function(){
    $(this).mask('0000000000');
  });

  $('#field-day').focus(function(){
    $(this).mask('00');
  });

  $('#field-month').focus(function(){
    $(this).mask('00');
  });

  $('#field-year').focus(function(){
    $(this).mask('0000');
  });


  //Captcha reload button code
  $('#reload_captcha').on('click', function(e){
    e.preventDefault();
    reload_captcha();
  })

  //Initialization code for field validation
  if($('#personal-info-form').length > 0){
    reload_captcha();
    $('#personal-info-form').validationEngine({'ajaxFormValidationMethod': 'post', scroll: false});
  }

  //Skill test form validation 
  if($('#skill_test_form').length > 0)
    $('#skill_test_form').validationEngine();

  //Skill test form countdown code init set to 5 mins.
  if($('#skill_test_form').length > 0)
    $('#countdown').countdown({until: '+4m +60s', format: 'mS', onExpiry: lockPage, compact: true, significant: 2}); 

  $('#skill_test_form').on('submit', function(e){
    $('#skill_test_button').prop('disabled', true);
  });

  if($('.autotab').length > 0 )
    $('.autotab').autotab();
  
  //Facebook share button 
  $('#facebook-share-btn').on('click', function(e){
    e.preventDefault();

    ga('send', 'event', 'button', 'click', 'Facebook', 1);

    var language = '';
    
    if($('#language_selector').html().trim() == 'English')
      language = 'fr';
    else
      language = 'en';
    
    share_contest(language);
  });

  $('input').placeholder();

 // if($('input[name=facebook]').val() == 'true') {
  
//}

  $('.popup').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });

  

  bajb_backdetect.OnBack = function()
  {
   //alert('back');
    setTimeout(redirect_on_back, 5);
  }
 
});


function redirect_on_back() {
  window.location.replace("/");
}


 // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {

      //console.log('statusChangeCallback');
      //console.log(response);

    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      fill_personal_info_form();
      //$('input[name=signed_request]').val(response.authResponse.signedRequest);
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      FB.login(function(login_response){
        if(login_response.status == 'connected')
          fill_personal_info_form();
      }, {scope: 'public_profile, email'});
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      FB.login(function(login_response){
        if(login_response.status == 'connected')
          fill_personal_info_form();
      }, {scope: 'public_profile, email'});
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '771789766176516',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.
if($('input[name=is_facebook]').val())
  checkLoginState();

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function fill_personal_info_form() {
    FB.api('/me', function(response) {
      
      if($('#personal-info-form').length > 0){
        $('input[name=facebook_id]').val(response.id);
        $('input[name=field-first-name]').val(response.first_name);
        $('input[name=field-last-name]').val(response.last_name);
        $('input[name=field-email]').val(response.email);
      }
      if($('#landing-page').length > 0 || $('#skill_test_form').length > 0)
        $('input[name=facebook_id]').val(response.id);
    });
  }

  // When 5 min countdown expires on skill test page this one locks and throwns a fail error when submitted.
  function lockPage () {
    $('input[name=expired]').val(1);
    window.location.replace("/home/sorry");

  }

  function share_contest(language) {
    if(language == 'fr'){
      share_title = 'Concours Dentyne';
      share_text = 'Je viens de participer au concours « DENTYNE - Rapprochements sans risques au cinéma » pour courir la chance de gagner un voyage à Hollywood!Aucun achat requis. Le concours prend fin le 26 août 2014 à midi HE. Réservé aux résidents légaux du Canada âgés de 16 ans et plus. Réponse à une question mathématique et CUP de produit requis. Certaines restrictions s’appliquent. Pour participer et voir les détails des prix, dont la VMA et les chances de gagner, visitez www.gagnezavecdentyne.ca';
      share_link = 'http://www.gagnezavecdentyne.ca';
    }
    else {
      share_title = 'Dentyne Contest';
      share_text = 'I just entered the DENTYNE \'Practice Safe Breath at the Movies\' contest for a chance to win a trip to Hollywood! No purchase necessary.  Ends Aug.26/2014 @ noon ET. Legal residents of Can 16+.  Math skill-test & product UPC req\'d.  Restrictions apply.  To enter & for prizing deets, incl. ARV & odds, go to www.winwithdentyne.ca';
      share_link = 'https://www.winwithdentyne.ca';
    }
    FB.ui({
    method: 'feed',
    name: share_title, // name of the product or content you want to share
    link: share_link, // link back to the product or content you are sharing
    picture: 'http://winwithdentyne.ca/css/images/home-bg.jpg', // path to an image you would like to share with this content
    caption: share_title, // caption
    description: share_text // description of your product or content
}, function(response){});
  }

  function reload_captcha() {
    $.ajax({
      url: '/home/create_captcha',
      type: 'post',
      success: function(response){
        if(response.status == 'SUCCESS'){
          $('#captcha_image').html(response.data);
        }
      }
    });
  }

